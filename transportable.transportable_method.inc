<?php
// $Id$
/*
 * @file transportable.transportable_method.inc
 * Provides node hooks for transportable_method node type
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 *
 */

/**
 * Hook form handler
*/
function transportable_node_transportable_method_form(&$form, &$node, $form_state) {
  //add to the form array
  $form['basic']['supplier_id'] = array(
    '#type' => 'select',
    '#title' => t('Supplier'),
    '#default_value' => $node->supplier_id,
    '#options' => travel_supplier_get_suppliers(),
    '#description' => t('Choose the supplier for this method'),
  );

  $form['seating'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Seating Details'),
    '#collapsed'    => FALSE,
    '#weight'       => -10,
    '#group'        => 'transportable',
  );

  $form['seating']['number_of_seats'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Number of seats'),
    '#default_value' => $node->number_of_seats,
    '#size'          => 60,
    '#description'   => t('Enter the number of seats on this vehicle'),
    '#maxlength'     => 10,
    '#required'      => TRUE,
    '#ahah' => array(
      'path' => 'transportable/ahah/transportable_method/seating/plan',
      'wrapper' => 'transportable-method-plan-wrapper',
      'event' => 'change',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  transportable_inc('util');

  $seating_type_default = (isset($form_state['values']['seat_type']) ? $form_state['values']['seat_type'] :
                           (isset($node->seat_type) ? $node->seat_type: 0));
  $form['seating']['seat_type'] = array(
    '#type'          => 'radios',
    '#title'         => t('Seat type'),
    '#default_value' => $seating_type_default,
    '#description'   => t('Choose the seat type for this transportable method'),
    '#options'       => _transportable_seat_types(),
    '#required'      => TRUE,
  );

  $seating_default = isset($node->free_seating) ? $node->free_seating: 1;
  $form['seating']['free_seating'] = array(
    '#type'          => 'radios',
    '#default_value' => $seating_default,
    '#title'       => t('Seating type'),
    '#description' => t('Choose the seating arrangement for this transportable method'),
    '#options'    => array(
      1 => 'Free seating',
      0 => 'Reserved seating'
    ),
    '#ahah' => array(
      'path' => 'transportable/ahah/transportable_method/seating/plan',
      'wrapper' => 'transportable-method-plan-wrapper',
      'event' => 'change',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  $form['#cache'] = TRUE;

  $seats_default = (isset($form_state['values']['number_of_seats']) ? $form_state['values']['number_of_seats'] : $node->number_of_seats);
  $seating_default = (isset($form_state['values']['free_seating']) ? $form_state['values']['free_seating'] :
                                                                         (isset($node->free_seating) ? $node->free_seating: 0));
  if ($seats_default && ($seating_default == 0)) {
    $form['seating']['plan'] = array(
      '#prefix' => '<div class="clear-block" id="transportable-method-plan-wrapper">',
      '#suffix' => '</div>',
      '#ahah_extra' => 'seating/',
      '#default_value' => (isset($form_state['values']['plan']) ? $form_state['values']['plan'] : $node->plan),
      '#type' => 'allocated_seating_seat_design',
      '#seats' => (isset($form_state['values']['number_of_seats']) ? $form_state['values']['number_of_seats'] : $node->number_of_seats)
    );
  }
  else {
    //add these here in case ahah adds our plan
    jquery_ui_add(array('ui.sortable', 'ui.tabs', 'ui.droppable'));
    drupal_add_js(drupal_get_path('module', 'allocated_seating') .'/allocated_seating.js');
    drupal_add_css(drupal_get_path('module', 'allocated_seating') .'/allocated_seating.css');
    drupal_add_css(drupal_get_path('module', 'jquery_ui') . '/jquery.ui/themes/default/ui.all.css');
    $form['seating']['plan'] = array(
      '#prefix' => '<div class="clear-block" id="transportable-method-plan-wrapper">',
      '#suffix' => '</div>',
      '#value' => t('This transportable method does not have reserved seating')
    );
  }
}

/**
 * Hook validate handler
*/
function transportable_node_transportable_method_validate($node, &$form) {
  //test the form
  if ($node->free_seating == 1 && !$node->plan) {
   // form_set_error('seating][wrap][plan_help', t('You must provide a reserved seating plan'));
  }
}

/**
 * Hook insert handler
*/
function transportable_node_transportable_method_insert($node) {
  //insert into db
  $node->seating_plan = serialize($node->plan);
  $result = drupal_write_record('transportable_method', $node);
}

/**
 * Hook update handler
*/
function transportable_node_transportable_method_update($node) {
  //do the update
  $update = array('vid');
  $node->seating_plan = serialize($node->plan);
  $result = drupal_write_record('transportable_method', $node, $update);
}

/**
 * Hook delete handler
*/
function transportable_node_transportable_method_delete(&$node) {
  //do the delete
  db_query("DELETE FROM {transportable_method} WHERE vid = %d", $node->vid);
}

/**
 * Hook load handler
*/
function transportable_node_transportable_method_load($node) {
  $data = db_fetch_object(db_query("SELECT * FROM {transportable_method}
                                   WHERE vid = %d", $node->vid));
  $data->plan = unserialize($data->seating_plan);
  return $data;
}

/**
 * Hook view handler
*/
function transportable_node_transportable_method_view(&$node, $teaser, $page) {
  //set up content
}