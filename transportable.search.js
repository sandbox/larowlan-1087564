//$Id$
/*
 * @file transportable.search.js
 * provides js to control showing/hiding the return trip fields
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

Drupal.behaviors.transportable_search = function(context) {
  $('input.transportable-search-return:not(.transportable-processed)').change(function(){
    var radio = $(this);
    var val = radio.val();
    var ret = $(radio).parents('.transportable-search-return').siblings('.transportable-return-wrap');
    if (val == '1') {
      ret.fadeIn();
    }
    else {
      ret.fadeOut();
    }
  }).addClass('transportable-processed')
}