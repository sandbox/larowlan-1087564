//$Id$
/*
 * @file transportable-service.js
 * Provides js to handle hiding/showing of form elements on edit form
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

Drupal.behaviors.transportable_service = function(context) {
  $('.transportable-service-stop-point-type:not(.transportable-processsed)').change(function(){
    var select = $(this);
    var val = parseInt(select.val());
    var enable = [];
    var disable = [];
    var row = select.parents('tr');
    switch (val) {
      case 0: //both
        enable.push('.transportable-service-stop-point-tickets-are-sold',
                     '.transportable-service-stop-point-is-meal-break',
                     '.transportable-service-stop-point-disembark-cd',
                     '.transportable-service-stop-point-disembark-type',
                     '.transportable-service-stop-point-embark-cd',
                     '.transportable-service-stop-point-embark-type');
        break;
      
      case 1: //embark only
        enable.push('.transportable-service-stop-point-embark-cd',
                     '.transportable-service-stop-point-embark-type');
        disable.push('.transportable-service-stop-point-tickets-are-sold',
                     '.transportable-service-stop-point-is-meal-break',
                     '.transportable-service-stop-point-disembark-cd',
                     '.transportable-service-stop-point-disembark-type');
        break;
      
      case 2: //disembark only
        enable.push('.transportable-service-stop-point-tickets-are-sold',
                     '.transportable-service-stop-point-is-meal-break',
                     '.transportable-service-stop-point-disembark-cd',
                     '.transportable-service-stop-point-disembark-type');
        disable.push('.transportable-service-stop-point-embark-cd',
                     '.transportable-service-stop-point-embark-type');
        break;
    }
    var enableQry = enable.join(',');
    var disableQry = disable.join(',');
    //enable items
    if (enableQry) {
      row.find(enableQry).attr('disabled', false);
    }
    //disable items
    if (disableQry) {
      row.find(disableQry).attr('disabled', true);
    }
  }).addClass('transportable-processed');
}