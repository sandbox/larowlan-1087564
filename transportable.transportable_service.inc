<?php
// $Id$
/*
 * @file transportable.transportable_service.inc
 * Provides node hooks for transportable_service node type
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html 
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
 * Hook form handler
 * @todo
*/
function transportable_node_transportable_service_form(&$form, &$node, $form_state) {
  //add to the form array
  transportable_inc('util');
  
  $form['basic']['tm_nid'] = array(
    '#type' => 'select',
    '#title' => t('Transport method'),
    '#default_value' => $node->tm_nid,
    '#options' => _transportable_get_transportable_methods(),
    '#description' => t('Choose the transportable method for this service'),
  );
  
  //the stop points table
  if (!$form_state['storage']['removed']) {
    $form_state['storage']['removed'] = array();
  }
  else {
    drupal_set_message(t('Changes to removed items will not be made permanent until save is pressed.'));
  }
  
  $form['#cache'] = TRUE;
  
 
  $items = $node->stop_points;
  if (empty($items)) {
    $items = array();
  }
  // the contents of $panes will either come from the db or from $form_state
  if (isset($form_state['items'])) {
    $items = $form_state['items'] + (array)$items;
  }  
  
  $form['stop_points'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Stop points'),
    '#collapsed'    => FALSE,
    '#weight'       => -10,
    '#group'        => 'transportable',
  );
  
  $form['stop_points']['items_wrapper'] = array(
    '#tree' => FALSE,
    '#weight' => -3,
    '#prefix' => '<div class="clear-block" id="transportable-service-stop-point-items-wrapper">',
    '#suffix' => '</div>',
  );

  $form['stop_points']['items_wrapper']['items'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="transportable-service-stop-point-items">',
    '#suffix' => '</div>',
    '#theme' => 'transportable_service_stop_point_form_items',
  );

  // Add the current panes to the form.
  foreach ($items as $delta => $item) {
    if (in_array($item['id'], $form_state['storage']['removed'])) {
      continue; //don't do the removed ones
    }
    $item['delta'] = $delta;
    $form['stop_points']['items_wrapper']['items'][$delta] = _transportable_service_stop_point_row($item);
  }
  
  $stop_points = _transportable_get_stop_points();
  //strip those in list already
  foreach ($items as $item) {
    unset($stop_points[$item['sp_nid']]);
  }
  
  $form['stop_points']['items_wrapper']['add_stop_point'] = array(
    '#type' => 'select',
    '#title' => t('Add stop point'),
    '#options' => $stop_points,
    '#description' => t('Choose a stop point to add to the service'),
  );
    
  $form['stop_points']['items_wrapper']['add_another'] = array(
    '#type' => 'submit',
    '#value' => t('Add to service'),
    '#attributes' => array('class' => 'transportable-service-stop-point-new-item', 'title' => t('Click here to add this new stop point.')),
    '#submit' => array('transportable_service_stop_point_add_submit'), // If no javascript action.
    '#ahah' => array(
      'path' => 'transportable/ahah/transportable_service/stop_points/items_wrapper',
      'wrapper' => 'transportable-service-stop-point-items-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  
  $form['concessions'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Concessions'),
    '#collapsed'    => FALSE,
    '#weight'       => -10,
    '#group'        => 'transportable',
    '#tree'         => TRUE
  );
  
  $form['concessions']['title'] = array(
    '#value' => '<div class="transportable-pane-caption">'. t('Choose which concession to use for each concession type') .'</div>'
  );
  
  transportable_inc('concessions');
  $concessions = _transportable_get_concessions(TRUE, TRUE);
  
  foreach (_transportable_concession_types() as $concession_type => $title) {
    $form['concessions'][$concession_type] = array(
      '#type' => 'radios',
      '#options' => $concessions,
      '#title' => $title,
      '#required' => TRUE,
      '#default_value' => $node->concessions[$concession_type]
    );
  }
  
}

/**
 * Utility to generate form items for a row in stop points table
 * @todo
*/
function _transportable_service_stop_point_row($item) {
  static $weights;//static for performance

  $form['#tree'] = TRUE;
  $delta = $item['delta'];

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $item['id']
  );

  $sp_node = node_load($item['sp_nid']);

  $form['title'] = array(
    '#value' => check_plain($sp_node->title),
  );
  
  $form['sp_nid'] = array(
    '#type' => 'value',
    '#value' => $item['sp_nid']
  );
  
  //stop_point_type
  $form['stop_point_type'] = array(
    '#type' => 'select',
    '#default_value' => $item['stop_point_type'],
    '#attributes' => array('class' => 'transportable-service-stop-point-type'),    
    '#options' => _transportable_stop_point_type()
  );
  
  //embark_type
  $form['embark_type'] = array(
    '#type' => 'select',
    '#attributes' => array('class' => 'transportable-service-stop-point-embark-type'),    
    '#default_value' => $item['embark_type'],
    '#options' => _transportable_embark_type(),
    '#disabled' => ($item['stop_point_type'] == 2)
  );
  
  //embark_cd
  $form['embark_cd'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#attributes' => array('class' => 'transportable-service-stop-point-embark-cd'),
    '#default_value' => $item['embark_cd'],
    '#maxlength' => 10,
    '#disabled' => ($item['stop_point_type'] == 2)
  );
  
  //disembark_type
  $form['disembark_type'] = array(
    '#type' => 'select',
    '#attributes' => array('class' => 'transportable-service-stop-point-disembark-type'),    
    '#default_value' => $item['disembark_type'],
    '#options' => _transportable_embark_type(),
    '#disabled' => ($item['stop_point_type'] == 1)
  );
  
  //disembark_cd
  $form['disembark_cd'] = array(
    '#type' => 'textfield',
    '#size' => 10,
    '#attributes' => array('class' => 'transportable-service-stop-point-disembark-cd'),
    '#default_value' => $item['disembark_cd'],
    '#maxlength' => 10,
    '#disabled' => ($item['stop_point_type'] == 1)
  );
    
  //is_meal_break
  $form['is_meal_break'] = array(
    '#type' => 'checkbox',
    '#title' => t('Meal break stop'),
    '#default_value' => $item['is_meal_break'],
    '#attributes' => array('class' => 'transportable-service-stop-point-is-meal-break'),
    '#disabled' => ($item['stop_point_type'] == 1)    
  );
  
  //tickets are sold
  $form['tickets_are_sold'] = array(
    '#type' => 'radios',
    '#options' => array(t('No'), t('Yes')),
    '#title' => t('Sell tickets to this stop point'),
    '#default_value' => isset($item['tickets_are_sold']) ? $item['tickets_are_sold'] : 0,
    '#attributes' => array('class' => 'transportable-service-stop-point-tickets-are-sold'),
    '#disabled' => ($item['stop_point_type'] == 1)
  );
  
  if (empty($weights)) {
    $weights = array();
    for ($i = -50; $i <= 50; $i++) {
      $weights[$i] = $i;
    }
  }
  
  //weight
  $form['weight'] = array(
    '#type' => 'select',
    '#default_value' => $item['weight'],
    '#options' => $weights
  );

  $form['remove'] = array(
    '#type' => 'submit',
    '#prefix' => '<div>',
    '#suffix' => '<label for="edit-remove">'. t('Remove') .'</label></div>',
    '#value' => 'remove_'. $delta,
    '#attributes' => array('class' => 'delete-item', 'title' => t('Click here to delete this item.')),
    '#submit' => array('transportable_service_stop_point_remove_submit'),
    '#ahah' => array(
      'path' => 'transportable/ahah/transportable_service/stop_points/items_wrapper',
      'wrapper' => 'transportable-service-stop-point-items-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );


  return $form;
}

/**
 * Submit handler for adding new stop point
*/
function transportable_service_stop_point_add_submit($form, &$form_state) {
  $items = _transportable_form_state_to_stop_points($form_state);
  $stop_point = $form_state['values']['add_stop_point'];
  if (!$stop_point) {
    drupal_set_message(t('You must choose a stop point to add'));
  }
  else {
    $sp_node = node_load($stop_point);
    if ($sp_node->type != 'stop_point') {
      drupal_set_message(t('You must choose a valid stop point to add'));      
    }
    else {
      // Add a new point at the end.
      $items[] = array(
        'sp_nid' => $stop_point
      );
    }
  }

  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
  $form_state['items'] = $items;
  $form_state['rebuild'] = TRUE;
}


/**
 * Submit handler for the "Remove stop point" button.
 */
function transportable_service_stop_point_remove_submit($form, &$form_state) {
  // Get the pane delta for the clicked button.
  $delta = $form_state['clicked_button']['#parents'][1];
  // Copy the form_state because unset() does not work on variable
  // that is passed by reference.
  $form_state_copy = $form_state;
  $remove = $form_state_copy['values']['items'][$delta];
  unset($form_state_copy['values']['items'][$delta]);
  $items = _transportable_form_state_to_stop_points($form_state_copy);

  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
  $form_state['items'] = $items;
  if (is_array($form_state['storage']['removed'])) {
    $form_state['storage']['removed'][] = $remove['id'];
  }
  else {
    $form_state['storage']['removed'] = array($remove['id']);
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Util to convert form_state to array of stop points
 * @param $form_state array form state variable from form handlers
*/
function _transportable_form_state_to_stop_points($form_state) {
  $items = $form_state['values']['items'];
  $weights = array();
  if (is_array($items)) {
    foreach ($items as $delta => $item) {
      $weights[$delta] = $item['weight'];
    }
    array_multisort($weights, SORT_ASC, $items); 
  }
  return $items;
}

/**
 * Hook validate handler
*/
function transportable_node_transportable_service_validate($node, &$form) {
  //test the form
  $validate = array('edit-submit', 'edit-save-continue', 'edit-preview');
  if (in_array($form_state['clicked_button']['#id'], $validate)) {
    if ($node->items && is_array($node->items)) {
      if (count($node->items) < 2) {
        form_set_error('', t('You must provide at least two stop points'));      
      }
      foreach ($node->items as $delta => $item) {
        //test items
        if ($item['embark_type'] && !$item['embark_cd']) {
          form_set_error('items]['. $delta .'][embark_cd', t('You must provide a corresponding gate/platform number.'));
        }
        if ($item['disembark_type'] && !$item['disembark_cd']) {
          form_set_error('items]['. $delta .'][disembark_cd', t('You must provide a corresponding gate/platform number.'));
        }
      }
    }
    else {
      form_set_error('', t('You must provide at least two stop points'));
    }
  }
}

/**
 * Hook insert handler
*/
function transportable_node_transportable_service_insert($node) {
  //insert into db
  $tm_node = node_load($node->tm_nid);
  if ($tm_node && $tm_node->supplier_id) {
    $node->supplier_id = $tm_node->supplier_id;
  }
  drupal_write_record('transportable_service', $node);
  foreach ($node->items as $delta => $item) {
    $item['ts_nid'] = $node->nid;
    drupal_write_record('transportable_service_stop_point', $item);
  }
  foreach ($node->concessions as $concession_type => $cid) {
    $record = array(
      'ts_nid' => $node->nid,
      'cid' => $cid,
      'concession_type' => $concession_type
    );
    drupal_write_record('transportable_service_concessions', $record);
  }
}

/**
 * Hook update handler
*/
function transportable_node_transportable_service_update($node) {
  //do the update
  $update = array('vid');
  drupal_write_record('transportable_service', $node, $update);
  foreach ($node->items as $delta => $item) {
    $update = array();
    //we have saved this before, update using id field
    if ($item['id']) {
      $update[] = 'id';
    }
    $item['ts_nid'] = $node->nid;
    drupal_write_record('transportable_service_stop_point', $item, $update);
  }
  db_query("DELETE FROM {transportable_service_concessions} WHERE ts_nid = %d", $node->nid);
  foreach ($node->concessions as $concession_type => $cid) {
    $record = array(
      'ts_nid' => $node->nid,
      'cid' => $cid,
      'concession_type' => $concession_type
    );
    drupal_write_record('transportable_service_concessions', $record);
  }
}

/**
 * Hook delete handler
*/
function transportable_node_transportable_service_delete(&$node) {
  //do the delete
  db_query("DELETE FROM {transportable_service} WHERE vid = %d", $node->vid);
  db_query("DELETE FROM {transportable_service_stop_point} WHERE ts_nid = %d", $node->nid);
  db_query("DELETE FROM {transportable_service_concessions} WHERE nid = %d", $node->nid);
}

/**
 * Hook load handler
*/
function transportable_node_transportable_service_load($node) {
  transportable_inc('util');
  $data = db_fetch_object(db_query("SELECT * FROM {transportable_service}
                                   WHERE vid = %d", $node->vid));
  $data->stop_points = _transportable_service_get_stop_points($node);
  
  if ($data->tm_nid) {
    $data->transportable_method = node_load($data->tm_nid);
  }
  
  $data->concessions = _transportable_service_get_concessions($node);
  
  return $data;
}

/**
 * Hook view handler
*/
function transportable_node_transportable_service_view(&$node, $teaser, $page) {
  //set up content
}

