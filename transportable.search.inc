<?php
// $Id$
/*
 * @file transportable.search.inc
 * Provides search handling stuff
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html 
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
 * Form builder for transportable search block
 * @param $form_state array std fapi form state var
 * @param $block boolean whether form is being displayed in block
*/
function transportable_search_form($form_state, $block = FALSE) {
  transportable_inc('util');
  if ($form_state['storage']['book'])  {
    //we are doing something with a booking here
    //show the seating plan/concession forms as required
    $form['book'] = array(
      '#theme' => 'transportable_book_form'
    );

    //store booking details for submit  
    $form['booking_details'] = array('#type' => 'value', '#value' => $form_state['storage']['book']);
    
    //form prep
    $multiples = FALSE;
    drupal_set_title(t('Choose you ticket and seating options'));
    
    //concession/ticket type
    $concessions = range(1, $form_state['values']['px']);
    
    $form['concessions'] = array(
      '#tree' => TRUE
    );
    
    $form['concessions']['caption'] = array(
      '#value' => '<div class="transportable-pane-caption">'.
        format_plural($form_state['values']['px'], 'Choose your ticket type', 'Choose the ticket type for each traveller') .'</div>'
    );
    
    $concession_options = _transportable_concession_types();
    $concession_options[-1] = t('Standard');
    #@todo compress concession options to reflect max percentage allowed
    
    foreach ($concessions as $person) {
      $form['concessions'][$person] = array(
        '#type' => 'select',
        '#title' => t('Traveller !count', array('!count' => $person)),
        '#required' => TRUE,
        '#default_value' => -1,
        '#options' => $concession_options
      );
    }
    
    if (is_array($form_state['storage']['book'])) {
      //does their ticket consist of multiples? (ie indirect)
      $multiples = TRUE;
      $form['title'] = array('#value' => '<div class="transportable-search-seating">'. t('Choose an option for each stage of your travel') .'</div>');
    }
    else {
      //this is a direct route but let't biff it into an array for consistent processing
      $form_state['storage']['book'] = array($form_state['storage']['book']);
    }
    //any of these tix feature reserved seating?
    $embark_types = _transportable_embark_type();
    $service_conditions = _transportable_service_condition();
    foreach ($form_state['storage']['book'] as $delta => $route) {
      list($service, $method, $reserved, $depart, $arrive, $from, $to) = _transportable_search_result_details($route, $service_conditions, $embark_types);
      if ($multiples && $reserved) { //don't need wraps if (a) no reserved seating or (b) no multiples
        $form['wrap_'. $delta] = array(
          '#type'         => 'fieldset',
          '#collapsible'  => TRUE,
          '#tree'         => FALSE,
          '#title'        => t('Seating from @from to @to', array(
                              '@from' => $from->title,
                              '@to' => $to->title
                              )),
          '#collapsed'    => FALSE,
          '#group'        => 'transportable',
        );
        
        //add seating plan
        $form['wrap_'. $delta]['seats'] = array(
          '#tree' => TRUE
        );
        $form['wrap_'. $delta]['seats'][$delta] = array(
          '#type' => 'allocated_seating_select',
          '#default_value' => $form_state['values']['seats'][$delta],
          '#required_seats' => $form_state['values']['px'],
          '#plan' => $method->plan
        );
      }
      elseif ($reserved) {
        //just add the plan (not in a vert tab as only one)
        $form['seats'] = array(
          '#tree' => TRUE
        );
        $form['required'][$delta] = array(
          '#type' => 'value',
          '#value' => $form_state['values']['px']
        );
        $reserved_seats_exist = TRUE;
        $form['seats'][$delta] = array(
          '#type' => 'allocated_seating_seat_select',
          '#default_value' => $form_state['values']['seats'][$delta],
          '#required_seats' => $form_state['values']['px'],
          '#plan' => $method->plan,
          '#not_available' => _transportable_service_instance_fetch_sold($route)
        );
      }
    }
    if ($reserved_seats_exist) {
      drupal_set_message(format_plural($form_state['values']['px'], 'Please choose 1 seat', 'Please choose @count seats'));
    }
    if ($multiples) { 
      //vertical tabs
      if (function_exists('vertical_tabs_form_pre_render')) {
        $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
      }
    }
    
    $form['reserve_seat'] = array('#type' => 'submit', '#value' => t('Reserve seat'));
    $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
  }
  else {
    if ($form_state['storage']['search_values']) {
      $form_state['values'] = $form_state['storage']['search_values'];
    }
    $form = array(
      '#action' => url('transportable/search'),
      '#cache' => TRUE,
      '#theme' => 'transportable_search_form',
      '#attributes' => array('class' => $block? 'transportable-search-block transportable-search-form' :  'transportable-search-form transportable-search-page')
    );
    
    //one-way/return
    $form['return'] = array(
      '#type' => 'radios',
      '#attributes' => array('class' => 'transportable-search-return'),
      '#default_value' => $form_state['values']['return'] ? $form_state['values']['return'] : 0,
      '#options' => array(0 => t('One way'), t('Return'))
    );
    
    transportable_inc('util');
    
    $stop_points = _transportable_stop_point_list();
    //from/to
    
    $form['from'] = array(
      '#type'          => 'select',
      '#title'         => t('From'),
      '#options'       => $stop_points,
      '#default_value' => $form_state['values']['from'] ? $form_state['values']['from'] : NULL,
      '#size'          => 1,
      '#attributes' => array('class' => 'transportable-search-from transportable-search-stop-point'),
      '#description'   => $block? '' : t('Where are you travelling from?'),
      '#required'      => TRUE,
    );
    $form['to'] = array(
      '#type'          => 'select',
      '#title'         => t('To'),
      '#options'       => $stop_points,
      '#default_value' => $form_state['values']['to'] ? $form_state['values']['to'] : NULL,
      '#size'          => 1,
      '#attributes' => array('class' => 'transportable-search-to transportable-search-stop-point'),
      '#description'   => $block? '' : t('Where are you travelling to?'),
      '#required'      => TRUE,
    );
    
    $px_options = drupal_map_assoc(range(1, 10));
    
    $time_options = _transportable_time_periods();
    
    //passengers
    $form['px'] = array(
      '#type'          => 'select',
      '#title'         => t('Passengers'),
      '#options'       => $px_options,
      '#default_value' => $form_state['values']['px'] ? $form_state['values']['px'] : NULL,
      '#size'          => 1,
      '#attributes' => array('class' => 'transportable-search-px'),
      '#description'   => $block? '' : t('How many are travelling in your party?'),
      '#required'      => TRUE,
    );
    
    //depart date/time
    $form['depart_dt'] = array(
      '#type' => 'date_popup',
      '#title' => t('Depart'),
      '#required' => TRUE,
      '#size' => 14,
      '#default_value' => $form_state['values']['depart_dt'] ? date('Y-m-d h:i:s', strtotime($form_state['values']['depart_dt'])) : date('Y-m-d h:i:s', strtotime('+ 1 day')),
      '#date_type' => DATE_ISO,
      '#attributes' => array('class' => 'transportable-search-depart-dt'),
      '#date_timezone' => date_default_timezone_name(),
      '#date_format' => variable_get('uc_date_format_default', 'd M Y'),
      '#date_year_range' => '-0:+1',
    );
    
    $form['depart_time'] = array(
      '#type'          => 'select',
      '#title'         => t('Time'),
      '#options'       => $time_options,
      '#default_value' => $form_state['values']['depart_time'] ? $form_state['values']['depart_time'] : '00:00:00;23:59:00',
      '#size'          => 1,
      '#attributes' => array('class' => 'transportable-search-depart-time'),
      '#required'      => TRUE,
    );
    
    //return date/time
    $form['return_dt'] = array(
      '#type' => 'date_popup',
      '#title' => t('Return'),
      '#required' =>  FALSE,
      '#size' => 14,
      '#pre_render' => array('property_nice_date_help'),
      '#attributes' => array('class' => 'transportable-search-return-dt'),
      '#default_value' => $form_state['values']['return_dt'] ? date('Y-m-d h:i:s', strtotime($form_state['values']['return_dt'])) : NULL,
      '#date_type' => DATE_ISO,
      '#date_timezone' => date_default_timezone_name(),
      '#date_format' => variable_get('uc_date_format_default', 'd M Y'),
      '#date_year_range' => '-0:+1',
    );
    
    $form['return_time'] = array(
      '#type'          => 'select',
      '#title'         => t('Time'),
      '#options'       => $time_options,
      '#default_value' => $form_state['values']['return_time'] ? $form_state['values']['return_time'] : '00:00:00;23:59:00',
      '#size'          => 1,
      '#attributes' => array('class' => 'transportable-search-return-time'),
      '#required'      => FALSE,
    );
    
    //search btn
    $form['search'] = array('#type' => 'submit', '#value' => t('Search'));
    
    //results
    if ($form_state['submitted']) {
      if ($form_state['storage']['return']) {
        //switch values in form_state for fetching results
        //it is safe to do this here b/c we are at the end of form processing
        $values = $form_state['values'];
        //switch to/from
        $form_state['values']['from'] = $values['to'];
        $form_state['values']['to'] = $values['from'];
        //switch dates
        $form_state['values']['depart_dt'] = $values['return_dt'];
        $form_state['values']['depart_time'] = $values['return_time'];
      }
      $form['results'] = transportable_search_results($form_state['values']);
      $form['results']['#theme'] = 'transportable_search_results';
      $form['results']['#search_return'] = $form_state['storage']['return'];
      $form['results']['#tree'] = TRUE;
    }
  }
  
  return $form;
}

/**
 * Validation
*/
function transportable_search_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!$form_state['storage']['book']) {
    //check from and to aren't the same (duh!)
    if ($values['from'] == $values['to']) {
      form_set_error('to', t('Your destination cannot be the same as your point of origin.'));
    }
    //check that we are returning after we depart (duh!)
    if ($values['return_dt']) {
      $return_dt = strtotime($values['return_dt']);
      $depart_dt = strtotime($values['depart_dt']);
      if ($return_dt < $depart_dt) {
        form_set_error('return_dt', t('Your return date cannot be before your departure date'));
      }
    }
  }
}

/**
 * Submit handler
*/
function transportable_search_form_submit($form, &$form_state) {
  if ($form_state['storage']['book']) {
    if ($form_state['values']['op'] == t('Reserve seat')) {
      transportable_inc('util');
      $book_details = $form_state['storage']['book'];
      unset($form_state['storage']['book']);//get this out so can't duplicate action by mistake
      $redirect = TRUE;
      if ($form_state['storage']['search_values']['return'] == 1 && !$form_state['storage']['return']) {
        //user want's return trip, don't redirect to cart, redirect to search results again
        //with return flag set so form builder knows to use return dates and swap the from/to
        $redirect = FALSE;
        $form_state['storage']['return'] = TRUE;
      }
      if ($form_state['storage']['return'] && $redirect) {
        //we are doing the add 'return to cart' and hence don't need this
        unset($form_state['storage']['search_values']);
        unset($form_state['storage']['return']);
      }
      if (is_object($book_details)) {
        //direct one, arrayify it
        $book_details = array($book_details);
      }
      foreach ($book_details as $details) {
        if ($form_state['values']['seats']) {
          //looking at reserved seats
          $c = 1;
          foreach ($form_state['values']['seats'] as $seat) {
            if (!$seat) {
              continue;
            }
            $sid = transportable_search_reserve_seat($details, $form_state['values']['concessions'][$c], $seat);
            $c++;
            if ($sid) {
              uc_cart_add_item($details->ts_nid, 1, array(
                'sids' => $sid,
                'module' => 'transportable',
                'details' => $details,
                'concession' => $form_state['values']['concessions'][$c]
              ), NULL, FALSE, FALSE);
            }
            else {
              drupal_set_message(t('Could not reserve one of your selected seats, another user has reserved it already.'));
            }
          }
        }
        else {
          foreach ($form_state['values']['concessions'] as $concession_type) {
            $sid = transportable_search_reserve_seat($details, $concession_type);
            if ($sid) {
              uc_cart_add_item($details->ts_nid, 1, array(
                'sids' => $sid,
                'module' => 'transportable',
                'details' => $details,
                'concession' => $concession_type
              ), NULL, FALSE, FALSE);
            }
            else {
              drupal_set_message(t('Could not reserve one of your seats, another user reserved the last available seats.'));
            }
          }
        }
      }
      drupal_set_message(t('Added your booking to the cart'));
      if ($redirect) {
        drupal_goto('cart');
      }
    }
    else {
      //user pressed cancel
      unset($form_state['storage']['book']);//get this out so can't duplicate action by mistake
      drupal_goto('transportable/search');
    }
  }
  //biff up form_state['storage'] so the form shows the book page instead
  //we can have direct or indirect here
  if ($form_state['clicked_button']['#parents'] &&
      is_array($form_state['clicked_button']['#parents']) &&
      $form_state['clicked_button']['#parents'][1] == 'book' &&
      ($type = $form_state['clicked_button']['#parents'][2]) &&
      is_numeric($form_state['clicked_button']['#parents'][3]) &&
      ($price_id = $form_state['clicked_button']['#parents'][3])) {
    if (($booking = $form['results']['rates'][$type][$price_id]['#value'])) {
      $form_state['storage']['book'] = $booking;
    }
    elseif (is_array($form['results']['rates'][$type][$price_id])) {
      $delta = $price_id;
      foreach (element_children($form['results']['rates'][$type][$delta]) as $price_id) {
        $booking[] = $form['results']['rates'][$type][$delta][$price_id]['#value'];
      }
      $form_state['storage']['book'] = $booking;
    }
    $form_state['storage']['search_values'] = $form_state['values'];
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Search result handler
 * @param $values array submitted form vars
 * @return array FAPI form array
*/
function transportable_search_results($values) {
  $form = array('rates' => array());
  
  transportable_inc('util');
  list($from_province, $from_city) = _transportable_stop_point_split($values['from']);
  list($to_province, $to_city) = _transportable_stop_point_split($values['to']);

  //get from stop point instances
  $froms = _transportable_get_location_stop_points($from_province, $from_city);
  
  //get to stop point instances
  $tos = _transportable_get_location_stop_points($to_province, $to_city);
  
  if (count($froms) == 0 || count($tos) == 0) {
    //no results
    return $form;
  }

  //build direct query
  #@todo what to do with return?
  $query = "SELECT tsspr.id,
          tsspr.tsi_id,
          sp_fr.sp_nid AS sp_from,
          sp_to.sp_nid AS sp_to,
          sp_fr.embark_type,
          sp_fr.embark_cd,
          sp_to.disembark_type,
          sp_to.disembark_cd,
          sp_fr.ts_nid,
          depart.depart,
          arrive.arrive,
          depart.service_condition,
          tsspr.rate
          FROM {transportable_service_stop_point_rate} tsspr
          INNER JOIN {transportable_service_stop_point} sp_fr
          ON tsspr.tssp_id_from = sp_fr.id
          INNER JOIN {transportable_service_stop_point} sp_to
          ON tsspr.tssp_id_to = sp_to.id
          INNER JOIN transportable_service_instance tsi ON
          tsspr.tsi_id = tsi.id
          INNER JOIN {transportable_service_stop_point_instance} depart ON
          tsspr.tsi_id = depart.tsi_id AND tsspr.tssp_id_from = depart.tssp_id
          INNER JOIN {transportable_service_stop_point_instance} arrive ON
          tsspr.tsi_id = arrive.tsi_id AND tsspr.tssp_id_to = arrive.tssp_id
          WHERE sp_fr.sp_nid IN (". db_placeholders($froms) .")
          AND sp_to.sp_nid IN (". db_placeholders($tos) .")
          AND depart.depart BETWEEN '%s' AND '%s'";
  
  $params = array_merge($froms, $tos, _transportable_time_periods_to_range($values['depart_dt'], $values['depart_time']));
  
  $res = db_query($query, $params);
  
  while ($row = db_fetch_object($res)) {
    //can't do this in sql @todo think of way to test this with sql
    if (!_transportable_seats_are_available($row->ts_nid, $row->tsi_id, $row->sp_from,
                                        $row->sp_to, $values['px'])) {
      continue;
    }
    $form['rates']['direct'][$row->id] = array('#value' => $row, '#type' => 'value');
    $form['book']['direct'][$row->id] = array('#type' => 'submit', '#value' => t('Book'));
  }
  if (!$form['rates']) {
    //we have no direct routes
    //get ugly and look for intermediates
    $paths = array();
    foreach ($froms as $from) {
      foreach ($tos as $to) {
        $path = transportable_search_fetch_path($from, $to);
        if (count($path) > 0) {
          $paths[] = $path;
        }
      }
    }
    
    foreach ($paths as $delta => $path) {
      $query = "SELECT tsspr.id,
          tsspr.tsi_id,
          sp_fr.sp_nid AS sp_from,
          sp_to.sp_nid AS sp_to,
          sp_fr.embark_type,
          sp_fr.embark_cd,
          sp_to.disembark_type,
          sp_to.disembark_cd,
          sp_fr.ts_nid,
          depart.depart,
          arrive.arrive,
          depart.service_condition,
          tsspr.rate
          FROM {transportable_service_stop_point_rate} tsspr
          INNER JOIN {transportable_service_stop_point} sp_fr
          ON tsspr.tssp_id_from = sp_fr.id
          INNER JOIN {transportable_service_stop_point} sp_to
          ON tsspr.tssp_id_to = sp_to.id
          INNER JOIN {transportable_service_stop_point_instance} depart ON
          tsspr.tsi_id = depart.tsi_id AND tsspr.tssp_id_from = depart.tssp_id
          INNER JOIN {transportable_service_stop_point_instance} arrive ON
          tsspr.tsi_id = arrive.tsi_id AND tsspr.tssp_id_to = arrive.tssp_id
          WHERE tsspr.id IN (". db_placeholders($path) .")";#@todo filter this by date and in an intelligent order
      
      $res = db_query($query, $path);
      while ($row = db_fetch_object($res)) {
        //can't do this in sql @todo think of way to test this with sql
        if (!_transportable_seats_are_available($row->ts_nid, $row->tsi_id, $row->sp_from,
                                            $row->sp_to, $values['px'])) {
          continue;
        }
        $row->path = $path;
        $form['rates']['indirect'][$delta][$row->id] = array('#value' => $row, '#type' => 'value');
        $form['book']['indirect'][$delta] = array('#type' => 'submit', '#value' => t('Book set'));
      }
    }
  }
  
  return $form;
}

/**
 * Build out stop point with associated stop points
 * @param $stop_point_id int stop point id to build from
 * @param $assoc array being built out
 * @return nested array
*/
function transportable_search_fetch_assoc($stop_point_id, &$processed) {
  $points = array();
  $res = db_query("SELECT tsspr.id, spto.sp_nid AS sp_to
                  FROM {transportable_service_stop_point_rate} tsspr
                  INNER JOIN {transportable_service_stop_point} fr
                  ON tsspr.tssp_id_from = fr.id
                  INNER JOIN {transportable_service_stop_point} spto
                  ON tsspr.tssp_id_to = spto.id
                  WHERE fr.sp_nid = %d
                  ORDER BY tsspr.id DESC", $stop_point_id); //DESC sort as more direct methods hit the db last
  while ($point = db_fetch_object($res)) {
    if (!in_array($point->sp_to, $processed)) {//prevent recursion
      $processed[] = $point->sp_to;
      $point->points = transportable_search_fetch_assoc($point->sp_to, $processed);
      $points[$point->sp_to] = $point;
    }
  }
  return $points;
}

/**
 * Recursively walk an array looking for a stop point id
 * @param $stop_point_from int the origin stop point
 * @param $stop_point_to int the destination stop point
 * @return array of parent path ids (ids from transportable_service_stop_point_rate)
*/
function transportable_search_fetch_path($stop_point_from, $stop_point_to) {
  $processed[] = $stop_point_from;
  $points = transportable_search_fetch_assoc($stop_point_from, $processed);
  $path = array();
  foreach ($points as $point) {
    $search = transportable_search_node($point, $path, $stop_point_to);
    if ($search) {
      return $path;
    }
  }
  //there was no matching path
  return FALSE;
}

/**
 * Recursive function to search for stop point
 * @param $point object the stop point node in the tree
 * @param $path array of matching parent ids from transportable_service_stop_point_rate
 * @param $stop_point_to int the id being sought
 * 
*/
function transportable_search_node($point, &$path, $stop_point_to) {
  //be optimistic that this node is on the path!
  $path[] = $point->id;
  foreach ($point->points as $id => $child) {
    if ($id == $stop_point_to) {
      //a child of this node is the sought point
      //add the child
      //return true
      $path[] = $child->id;
      return TRUE;
    }
    else {
      //try the grandchildren
      $search = transportable_search_node($child, $path, $stop_point_to);
      //one of the grand or great grandchildren worked!
      if ($search) {
        return TRUE;
      }
    }
  }
  //remove our path, we failed here
  array_pop($path);
  return FALSE;
}


