<?php
// $Id$
/*
 * @file transportable.concessions.inc
 * Provides concessions editing form and handlers
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html   
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

function transportable_concessions_form($form_state) {
  $form = array();
  //load util functions
  transportable_inc('util');
  
  if (!$form_state['storage']['removed']) {
    $form_state['storage']['removed'] = array();
  }
  else {
    drupal_set_message(t('Changes to removed items will not be made permanent until save is pressed.'));
  }
  
  $form['#cache'] = TRUE;
  
 
  $items = _transportable_get_concessions();
  // the contents of $panes will either come from the db or from $form_state
  if (isset($form_state['items'])) {
    $items = $form_state['items'] + (array)$items;
  }
  
  $form['items_wrapper'] = array(
    '#tree' => FALSE,
    '#weight' => -3,
    '#prefix' => '<div class="clear-block" id="transportable-concession-items-wrapper">',
    '#suffix' => '</div>',
  );

  $form['items_wrapper']['items'] = array(
    '#tree' => TRUE,
    '#prefix' => '<div id="transportable-concession-items">',
    '#suffix' => '</div>',
    '#theme' => 'transportable_concession_form_items',
  );
  
  if (count($items) == 0 ||
      count($items) == count($form_state['storage']['removed'])) {
    //we have no items or all are to be removed
    $items = array(
      0 => array(),
      1 => array(),
    );
  }

  // Add the current panes to the form.
  foreach ($items as $delta => $item) {
    if (in_array($item['cid'], $form_state['storage']['removed'])) {
      continue; //don't do the removed ones
    }
    $item['delta'] = $delta;
    $form['items_wrapper']['items'][$delta] = _transportable_concession_row($item);
  }
  
    
  $form['add_another'] = array(
    '#type' => 'submit',
    '#value' => t('Add another'),
    '#attributes' => array('class' => 'transportable-concession-new-item', 'title' => t('Click here to add new concession.')),
    '#submit' => array('transportable_concessions_add_submit'), // If no javascript action.
    '#ahah' => array(
      'path' => 'transportable/ahah/concessions/items_wrapper/items',
      'wrapper' => 'transportable-concession-items-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  
  //the save button
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  $form['#action'] = url('admin/store/settings/transportable/concessions');

  return $form;
}

/**
 * Utility to generate form items for a row in concessions table
*/
function _transportable_concession_row($item) {
  $form['#tree'] = TRUE;
  $delta = $item['delta'];

  $form['cid'] = array(
    '#type' => 'value',
    '#value' => $item['cid']
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#maxlength' => 50,
    '#size' => '10',
    '#default_value' => isset($item['title']) ? $item['title'] : '',
  );
  
  $form['age'] = array(
    '#type' => 'textfield',
    '#maxlength' => 3,
    '#size' => '4',
    '#attributes' => array('class' => 'transportable-small-field'),
    '#default_value' => isset($item['age']) ? $item['age'] : '',
  );
  
  $form['discount_percent'] = array(
    '#type' => 'textfield',
    '#maxlength' => 5,
    '#size' => '4',
    '#default_value' => isset($item['discount_percent']) ? $item['discount_percent'] : '',
    '#suffix' => '%'
  );
  
  $form['max_percent'] = array(
    '#type' => 'textfield',
    '#maxlength' => 5,
    '#size' => '4',
    '#default_value' => isset($item['max_percent']) ? $item['max_percent'] : '',
    '#suffix' => '%',
    '#attributes' => array('class' => 'transportable-small-field'),
  );
  
  $form['max_type'] = array(
    '#type' => 'select',
    '#default_value' => $item['max_type'],
    '#options' => _transportable_concession_max_type()
  );

  $form['age_criteria'] = array(
    '#type' => 'select',
    '#default_value' => $item['age_criteria'],
    '#options' => _transportable_concession_age_criteria()
  );
  
  $form['concession_type'] = array(
    '#type' => 'select',
    '#default_value' => $item['concession_type'],
    '#options' => _transportable_concession_types()
  );
  
  $form['supplier_id'] = array(
    '#type' => 'select',
    '#default_value' => $item['supplier_id'],
    '#options' => travel_supplier_get_suppliers()
  );

  $form['remove'] = array(
    '#type' => 'submit',
    '#prefix' => '<div>',
    '#suffix' => '<label for="edit-remove">'. t('Remove') .'</label></div>',
    '#value' => 'remove_'. $delta,
    '#attributes' => array('class' => 'delete-item', 'title' => t('Click here to delete this item.')),
    '#submit' => array('transportable_concessions_remove_submit'),
    '#ahah' => array(
      'path' => 'transportable/ahah/concessions/items_wrapper/items',
      'wrapper' => 'transportable-concession-items-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );


  return $form;
}

/**
 * Submit handler for transportable_concessions_form
*/
function transportable_concessions_form_submit($form, &$form_state) {
  //only submit if we are saving the form
  if ($form_state['clicked_button']['#id'] == 'edit-submit') {
    $items = _transportable_form_state_to_concessions($form_state);
    //do insert
    $res = TRUE;
    foreach ($items as $item) {
      $update = array('cid');
      if (!$item['cid']) {
        $update = array();
      }
      $res = $res && drupal_write_record('transportable_concessions', $item, $update);
    }
    if ($res) {
      drupal_set_message(t('Saved your concession types'));
    }
    else {
      drupal_set_message(t('An error occured whilst saving your concession types'));
    }
    if ($form_state['storage']['removed'] &&
        is_array($form_state['storage']['removed'])) {
      foreach ($form_state['storage']['removed'] as $remove) {
        db_query("DELETE FROM {transportable_concessions}
                 WHERE cid = %d", $remove);
      }
      $form_state['storage']['removed'] = NULL;
    }
  }
}

/**
 * Validation handler for transportable_concessions_form
*/
function transportable_concessions_form_validate($form, &$form_state) {
  //only validate if we are saving the form
  if ($form_state['clicked_button']['#id'] == 'edit-submit') {
    $items = _transportable_form_state_to_concessions($form_state);
    //test items
    foreach ($items as $j => $item) {
      if (empty($item['title'])) {
        form_set_error('items]['. $j .'][title', t('Title is required for each item.'));
      }
      if ($item['age_criteria'] && !$item['age']) {
        form_set_error('items]['. $j .'][age', t('You must provide an age when an age criteria is nominated.'));
      }
      if (empty($item['discount_percent'])) {
        form_set_error('items]['. $j .'][discount_percent', t('Price deduction is required for each item.'));        
      }
      if (empty($item['max_percent'])) {
        form_set_error('items]['. $j .'][max_percent', t('You must provide a maximum percentage for each item.'));        
      }
    }
  }
}

/**
 * Submit handler for adding new concession type
*/
function transportable_concessions_add_submit($form, &$form_state) {
  $items = _transportable_form_state_to_concessions($form_state);
  // Add an empty tab at the end.
  $items[] = array();

  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
  $form_state['items'] = $items;
  $form_state['rebuild'] = TRUE;
}


/**
 * Submit handler for the "Remove concession" button.
 */
function transportable_concessions_remove_submit($form, &$form_state) {
  // Get the pane delta for the clicked button.
  $delta = $form_state['clicked_button']['#parents'][1];
  // Copy the form_state because unset() does not work on variable
  // that is passed by reference.
  $form_state_copy = $form_state;
  $remove = $form_state_copy['values']['items'][$delta];
  unset($form_state_copy['values']['items'][$delta]);
  $items = _transportable_form_state_to_concessions($form_state_copy);

  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
  $form_state['items'] = $items;
  if (is_array($form_state['storage']['removed'])) {
    $form_state['storage']['removed'][] = $remove['cid'];
  }
  else {
    $form_state['storage']['removed'] = array($remove['cid']);
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Util to convert form_state to array of concessions
 * @param $form_state array form state variable from form handlers
*/
function _transportable_form_state_to_concessions($form_state) {
  return $form_state['values']['items'];
}

/*
 * Returns array of panes of form
 * array('title' =>, 'percent' => etc)
 * @param $assoc boolean use cid as keys?
 * @param $title boolean title only (when $assoc = TRUE)
 * @see table transportable_concessions
 */

function _transportable_get_concessions($assoc = FALSE, $title = FALSE) {
  $items = array();
  //@todo remove dependency on this module
  $suppliers = array_keys(travel_supplier_get_suppliers());
  $res = db_query("SELECT * FROM {transportable_concessions}
                  WHERE supplier_id
                  IN (". db_placeholders($suppliers) .")", $suppliers); 
  while ($item = db_fetch_array($res)) {
    if ($assoc) {
      if ($title) {
        $items[$item['cid']] = $item['title'];
      }
      else {
        $items[$item['cid']] = $item;        
      }
    }
    else {
      $items[] = $item;      
    }
  }
  return $items;
}