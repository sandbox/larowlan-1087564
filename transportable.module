<?php
// $Id$
/*
 * @file transportable.module
 * Provides transportable booking functionality for Ubercart
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * @todo conditional actions to move seats from provisional to sold
 */

//load external files with hooks
transportable_inc('ca');

/**
 * Implementation of hook_menu().
 */
function transportable_menu() {
  // admin screen
  $items['admin/store/settings/transportable'] = array(
    'title' => 'Transport settings',
    'page callback' => 'drupal_get_form',
    'access arguments' => array('Administer transportable settings'),
    'page arguments' => array('transportable_admin_form'),
    'file'           => 'transportable.admin.inc',
  );

  $items['admin/store/settings/transportable/settings'] = array(
    'title' => 'Transport settings',
    'page callback' => 'drupal_get_form',
    'access arguments' => array('Administer transportable settings'),
    'page arguments' => array('transportable_admin_form'),
    'type'        => MENU_DEFAULT_LOCAL_TASK
  );

  //generic ahah handler, append args to return specific form elements
  $items['transportable/ahah'] = array(
    'title'            => 'Transport generic ahah handler',
    'page callback'    => 'transportable_ahah',
    'access arguments' => array('create content'),
    'type'             => MENU_CALLBACK
  );

  //search page
  $items['transportable/search'] = array(
    'title'            => 'Search for transportable',
    'page callback'    => 'drupal_get_form',
    'file'             => 'transportable.search.inc',
    'page arguments'   => array('transportable_search_form'),
    'access arguments' => array('create content'),
    'type'             => MENU_CALLBACK
  );

  //concession settings
  $items['admin/store/settings/transportable/concessions'] = array(
    'title' => 'Transport concessions',
    'page callback' => 'drupal_get_form',
    'access arguments' => array('Administer concession settings'),
    'file' => 'transportable.concessions.inc',
    'page arguments' => array('transportable_concessions_form')
  );

  //staff permissions settings
  $items['admin/store/settings/transportable/permissions'] = array(
    'title' => 'Transport permissions',
    'page callback' => 'drupal_get_form',
    'access arguments' => array('Administer transportable staff permissions'),
    'file' => 'transportable.permissions.inc',
    'page arguments' => array('transportable_permissions_form')
  );

  //service schedule panes
  $items['node/%node/schedule'] = array(
    'title' => 'Schedule',
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'transportable_service_schedule_access',
    'page callback' => 'transportable_service_schedule',
    'access arguments' => array(1),
    'file' => 'transportable.schedule.inc',
    'page arguments' => array(1)
  );

  //service schedule instances pane
  $items['node/%node/schedule/instances'] = array(
    'title' => 'Schedule',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'access callback' => 'transportable_service_schedule_access',
    'page callback' => 'transportable_service_schedule',
    'weight' => 1,
    'access arguments' => array(1),
    'file' => 'transportable.schedule.inc',
    'page arguments' => array(1)
  );

  //service schedule add instance panes
  $items['node/%node/schedule/add'] = array(
    'title' => 'Add schedule instance',
    'type' => MENU_LOCAL_TASK,
    'access callback' => 'transportable_service_schedule_access',
    'page callback' => 'drupal_get_form',
    'access arguments' => array(1),
    'weight' => 2,
    'file' => 'transportable.schedule.inc',
    'page arguments' => array('transportable_service_schedule_instance_form', 1)
  );

  //service schedule edit instance pane
  $items['node/%node/schedule/%transportable_schedule_instance/edit'] = array(
    'title' => 'Edit schedule instance',
    'type' => MENU_CALLBACK,
    'access callback' => 'transportable_service_schedule_access',
    'page callback' => 'drupal_get_form',
    'access arguments' => array(1),
    'file' => 'transportable.schedule.inc',
    'page arguments' => array('transportable_service_schedule_instance_form', 1, 3)
  );

  //service schedule offline tix for instance pane
  $items['node/%node/schedule/%transportable_schedule_instance/offline'] = array(
    'title' => 'Offline sales',
    'type' => MENU_CALLBACK,
    'access callback' => 'transportable_service_schedule_access',
    'page callback' => 'drupal_get_form',
    'access arguments' => array(1),
    'file' => 'transportable.schedule.inc',
    'page arguments' => array('transportable_service_offline_sales_form', 1, 3) #@todo
  );

  //service schedule delete instance
  $items['node/%node/schedule/%transportable_schedule_instance/delete'] = array(
    'title' => 'Delete schedule instance',
    'type' => MENU_CALLBACK,
    'access callback' => 'transportable_service_schedule_access',
    'page callback' => 'drupal_get_form',
    'access arguments' => array(1),
    'file' => 'transportable.schedule.inc',
    'page arguments' => array('transportable_service_schedule_instance_delete_form', 1, 3)
  );

  $items['user/%user/transportable'] = array(
    'title' => 'Transport',
    'page callback' => 'transportable_user_admin_overview',
    'access arguments' => array('create Transport Service'),
    'file' => 'transportable.admin.inc',
    'page arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}


/**
 * Implementation of hook_perm().
 */
function transportable_perm() {
  return array(
    'create Transport Method',
    'edit own Transport Method',
    'edit any Transport Method',
    'create Transport Service',
    'edit own Transport Service',
    'edit any Transport Service',
    'edit own Stop Points',
    'edit any Stop Points',
    'Administer transportable settings',
    'Create universal stop points',
    'create Stop Points',
    'Administer concession settings',
    'Administer transportable staff permissions'
  );
}


/**
 * Implementation of hook_block().
 */
function transportable_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {

    case 'list':
      $blocks[0]['info'] = t('Transport search block');
      return $blocks;

    case 'view':
      if ($delta == 0) {
        transportable_inc('search');
        $block['subject'] = t('Transport Search');
        $block['content'] = drupal_get_form('transportable_search_form', TRUE);
      }
      return $block;
  }
}


/**
 * Implementation of hook_nodeapi().
 * /
function transportable_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'alter':
      // OPTIONAL: the $node->content array has been rendered, so the node body or
      // teaser is filtered and now contains HTML. This op should only be used when
      // text substitution, filtering, or other raw text operations are necessary.
      break;
    case 'delete':
      // OPTIONAL: The node is being deleted.
      break;
    case 'delete revision':
      // OPTIONAL: The revision of the node is deleted. You can delete data
      // associated with that revision.
      break;
    case 'insert':
      // OPTIONAL: The node is being created (inserted in the database).
      break;
    case 'load':
      // OPTIONAL: The node is about to be loaded from the database. This hook
      // can be used to load additional data at this time.
      break;
    case 'prepare':
      // OPTIONAL: The node is about to be shown on the add/edit form.
      break;
    case 'prepare translation':
      // OPTIONAL: The node is being cloned for translation. Load
      // additional data or copy values from $node->translation_source.
      break;
    case 'print':
      // OPTIONAL: Prepare a node view for printing. Used for printer-friendly
      // view in book_module
      break;
    case 'rss item':
      // OPTIONAL: An RSS feed is generated. The module can return properties
      // to be added to the RSS item generated for this node. See comment_nodeapi()
      // and upload_nodeapi() for examples. The $node passed can also be modified
      // to add or remove contents to the feed item.
      break;
    case 'search result':
      // OPTIONAL: The node is displayed as a search result. If you
      // want to display extra information with the result, return it.
      break;
    case 'presave':
      // OPTIONAL: The node passed validation and is about to be saved. Modules may
      //  use this to make changes to the node before it is saved to the database.
      break;
    case 'update':
      // OPTIONAL: The node is being updated.
      break;
    case 'update index':
      // OPTIONAL: The node is being indexed. If you want additional
      // information to be indexed which is not already visible through
      // nodeapi "view", then you should return it here.
      break;
    case 'validate':
      // OPTIONAL: The user has just finished editing the node and is
      // trying to preview or submit it. This hook can be used to check
      // the node data. Errors should be set with form_set_error().
      break;
    case 'view':
      // OPTIONAL: The node content is being assembled before rendering. The module
      // may add elements $node->content prior to rendering. This hook will be
      // called after hook_view().  The format of $node->content is the same as
      // used by Forms API.
      break;
  }
}


/**
 * Implementation of hook_theme().
 */
function transportable_theme($existing, $type, $theme, $path) {
  return array(
    'seating_plan_edit' => array(
      'arguments' => array('form' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'seating_plan_select' => array(
      'arguments' => array('form' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'transportable_cart_item_detail' => array(
      'arguments' => array('details' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'transportable_disclaimer_approval_form' => array(
      'arguments' => array('form' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'transportable_concession_form_items' => array(
      'arguments' => array('form' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'transportable_service_stop_point_form_items' => array(
      'arguments' => array('form' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'transportable_search_results' => array(
      'arguments' => array('form' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'transportable_search_form' => array(
      'arguments' => array('form' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'transportable_search_book_form' => array(
      'arguments' => array('form' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'transportable_offline_sales_table' => array(
      'arguments' => array('form' => NULL),
      'file' => 'transportable.theme.inc'
    ),
    'transportable_admin_links' => array(
      'arguments' => array('links' => NULL),
      'file' => 'transportable.theme.inc'
    ),
  );
}


/**
 * Implementation of hook_views_api().
 */
function transportable_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'transportable'),
  );
}


/**
 * Implementation of hook_node_info().
 */
function transportable_node_info() {

  return array(
    'transportable_method' => array(
      'name' => 'Transport Method',
      'module' => 'transportable',
      'description' => 'Provides a transportable method (eg vehicle/train/plane) for which transportable is bookable',
      'help' => 'Give your transportable method a name and enter the seating details',
      'has_title' => TRUE,
      'title_label' => 'Method name',
      'has_body' => TRUE,
      'body_label' => 'Description of transportable',
      'min_word_count' => 0,
      'locked' => FALSE,
    ),

    'transportable_service' => array(
      'name' => 'Transport Service',
      'module' => 'transportable',
      'description' => 'Provides a transportable service/route (eg set of stop points and a schedule) for which transportable is bookable',
      'help' => 'Give your transportable service a name and enter the stop point and schedule details',
      'has_title' => TRUE,
      'title_label' => 'Service name',
      'has_body' => TRUE,
      'body_label' => 'Description of transportable',
      'min_word_count' => 0,
      'locked' => FALSE,
    ),

    'stop_point' => array(
      'name' => 'Stop Point',
      'module' => 'transportable',
      'description' => 'Provides a transportable stop point to form part of a larger transportable service',
      'help' => 'Give your stop point a name and description then choose it\'s position on the map',
      'has_title' => TRUE,
      'title_label' => 'Stop point name',
      'has_body' => TRUE,
      'body_label' => 'Description of stop point',
      'min_word_count' => 0,
      'locked' => FALSE,
    ),
    // add further types as needed
  );
}


/**
 * Implementation of hook_access().
 * @todo multi-user permissions
 */
function transportable_access($op, $node, $account) {
  global $user;

  $type = node_get_types('type', $node);

  if ($op == 'create') {
    return user_access('create '. $type->title_label);
  }

  if ($op == 'update' || $op == 'delete') {
    if ((user_access('edit own '. $type->title_label) && ($user->uid == $node->uid))
        || user_access('edit any '. $type->title_label)) {
      return TRUE;
    }
  }
}


/**
 * Implementation of hook_form().
 */
function transportable_form(&$node, $form_state) {
  // The site admin can decide if this node type has a title and body, and how
  // the fields should be labeled. We need to load these settings so we can
  // build the node form correctly.
  $type = node_get_types('type', $node);

  $form['basic'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Basic Details'),
    '#collapsed'    => FALSE,
    '#weight'       => -11,
    '#group'        => 'transportable',
  );

  if ($type->has_title) {
    $form['basic']['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => TRUE,
      '#default_value' => $node->title,
      '#weight' => -5
    );
  }

  if ($type->has_body) {
    // In Drupal 6, we can use node_body_field() to get the body and filter
    // elements. This replaces the old textarea + filter_form() method of
    // setting this up. It will also ensure the teaser splitter gets set up
    // properly.
    $form['basic']['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }

  if (function_exists('vertical_tabs_form_pre_render')) {
    $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
  }

  $type_name = $node->type;
  //call private form builder for this type (we have multiple types)
  //load includes first
  transportable_inc($type_name);
  $function = 'transportable_node_'. $type_name .'_form';
  if (function_exists($function)) {
    $function($form, $node, $form_state);
  }

  return $form;
}


/**
 * Implementation of hook_validate().
 */
function transportable_validate($node, &$form) {
  $type = $node->type;
  transportable_inc($type);
  $function = 'transportable_node_'. $type .'_validate';
  if (function_exists($function)) {
    $function($node, $form);
  }
}


/**
 * Implementation of hook_insert().
 */
function transportable_insert($node) {
  $type = $node->type;
  transportable_inc($type);
  $function = 'transportable_node_'. $type .'_insert';
  if (function_exists($function)) {
    $function($node);
  }
}


/**
 * Implementation of hook_update().
 */
function transportable_update($node) {
  // if this is a new node or we're adding a new revision,
  if ($node->revision) {
    transportable_insert($node);
  }
  else {
    $type = $node->type;
    transportable_inc($type);
    $function = 'transportable_node_'. $type .'_update';
    if (function_exists($function)) {
      $function($node);
    }
  }
}


/**
 * Implementation of hook_delete().
 */
function transportable_delete(&$node) {
  $type = $node->type;
  transportable_inc($type);
  $function = 'transportable_node_'. $type .'_delete';
  if (function_exists($function)) {
    $function($node);
  }
}


/**
 * Implementation of hook_load().
 */
function transportable_load($node) {
  $type = $node->type;
  transportable_inc($type);
  $function = 'transportable_node_'. $type .'_load';
  if (function_exists($function)) {
    $return = $function($node);
  }
  return $return;
}


/**
 * Implementation of hook_view().
 */
function transportable_view($node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);
  $type = $node->type;
  transportable_inc($type);
  $function = 'transportable_node_'. $type .'_view';
  if (function_exists($function)) {
    $function($node, $teaser, $page);
  }

  return $node;
}


/**
 * Implementation of hook_menu_alter().
 */
function transportable_menu_alter(&$items) {

}


/**
 * Implementation of hook_form_alter().
 */
function transportable_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'transportable_method_node_form':
      $form['basic']['taxonomy'] = $form['taxonomy'];
      unset($form['taxonomy']);
      break;

    case 'stop_point_node_form':
      $form['location']['locations'] = $form['locations'];
      unset($form['locations']);
      break;
  }
}


/**
 * Implementation of hook_forms().
 */
function transportable_forms($form_id, $args) {

}


/**
 * Implementation of hook_init().
 */
function transportable_init() {
  //allow translation of disclaimer
  global $conf;
  $conf['i18n_variables'][] = 'transportable_checkout_disclaimer';
  $conf['i18n_variables'][] = 'transportable_service_conditions';
}


/**
 * Implementation of hook_theme_registry_alter().
 */
function transportable_theme_registry_alter(&$theme_registry) {
  $theme_registry['date_popup']['function'] = 'theme_date_popup_transportable';
}


/**
 * Implementation of hook_user().
 */
function transportable_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'load':
      $transportable = db_fetch_object(db_query("SELECT * FROM
                                   {transportable_permissions}
                                   WHERE uid = %d", $account->uid));
      $transportable->permissions = unserialize($transportable->permissions);
      $account->transportable_permissions = $transportable;
      break;
  }
}


/**
 * Implementation of hook_mail().
 */
function transportable_mail($key, &$message, $params) {

}


/**
 * Implementation of hook_cart_display().
 */
function transportable_cart_display($item) {
  $node = node_load($item->nid);
  $element = array();
  $element['nid'] = array('#type' => 'value', '#value' => $node->nid);
  $element['module'] = array('#type' => 'value', '#value' => 'transportable');
  $element['remove'] = array('#type' => 'checkbox');
  $element['title'] = array(
    '#value' => node_access('view', $node) ? l($item->title, 'node/'. $node->nid) : check_plain($item->title),
  );

  if ($item->data['concession'] != '-1') {
    $concession_types = _transportable_concession_types();
    $element['title']['#value'] .= ' ('. $concession_types[$item->data['concession']] .')';
  }

  $context = array(
    'revision' => 'altered',
    'type' => 'cart_item',
    'subject' => array(
      'cart_item' => $item,
      'node' => $node,
    ),
  );

  transportable_inc('util');
  $details = _transportable_cart_item_details($context, $item);

  $price_info = array(
    'price' => $details['total'],
    'qty' => $item->qty,
  );

  $element['#total'] = uc_price($price_info, $context);
  $element['data'] = array('#type' => 'hidden', '#value' => serialize($item->data));

  $element['qty'] = array(
    '#type' => 'textfield',
    '#default_value' => $item->qty,
    '#size' => 5,
    '#disabled' => FALSE,
    '#maxlength' => 6
  );

  $element['description'] = array('#value' => theme('transportable_cart_item_detail', $details['details']));

  return $element;
}


/**
 * Implementation of hook_cart_item().
 */
function transportable_cart_item($op, &$item) {
  switch ($op) {
    case 'load':
      if ($item->module == 'transportable') {
        $context = array(
          'revision' => 'altered',
          'type' => 'cart_item',
          'subject' => array(
            'cart_item' => $item,
            'node' => node_load($item->nid),
          ),
        );

        transportable_inc('util');
        $details = _transportable_cart_item_details($context, $item);
        $qty = count($item->data['tickets']);
        if (!$qty) {
          $qty = 1;
        }
        $item->qty = $qty;
        $item->price = $details['total'];
        $item->data['shippable'] = FALSE;
      }
      break;

    case 'can_ship':
      return FALSE;
  }
}


/**
 * Implementation of hook_cart_pane().
 */
function transportable_cart_pane($items) {
  transportable_inc('util');

  //no need if no tix in cart
  if (!transportable_item_in_cart()) {
    return array();
  }

  $durations = _transportable_durations();

  //convert both to minutes
  $conversion = array(
    'minutes' => 1,
    'hours' => 60,
    'days' => 60 * 24,
    'weeks' => 60 * 24 * 7,
    'years' => 60 * 24 * 365.24,
  );

  //work out which is less - held time or cart time
  $held_time = variable_get('transportable_save_tickets_duration', 1);
  $held_unit = variable_get('transportable_save_tickets_units', 'hours');

  $cart_time = variable_get('uc_cart_anon_duration', '4');
  $cart_unit = variable_get('uc_cart_anon_unit', 'hours');

  $held_minutes = $held_time * $conversion[$held_unit];
  $cart_minutes = $cart_time * $conversion[$cart_unit];

  $held = $cart_minutes;
  $unit = $cart_unit;
  if ($held_minutes < $cart_minutes) {
    $held = $held_minutes;
    $unit = $held_unit;
  }

  $message = format_plural(
    $held,
    'Please note that your seating will be temporarily reserved for 1 @unit.<br />
    To confirm your seat permanently please complete your order.',
    'Please note that your seating will be temporarily reserved for @count @units.<br />
    To confirm your seat permanently, please complete your order.',
    array(
      '@unit' => $unit,
      '@units' => $unit
    )
  );

  $panes[] = array(
    'id' => 'transportable_seating_warning',
    'title' => t('Seating warning'),
    'enabled' => TRUE,
    'weight' => -1,
    'body' => (!is_null($items) && transportable_item_in_cart()) ? '<div id="transportable-seating-warning">'. $message .'</div>': '',
  );

}


/**
 * Implementation of hook_checkout_pane().
 */
function transportable_checkout_pane() {
  $panes = array();
  //for the approval statement
  $panes[] = array(
    'id' => 'transportable_disclaimer',
    'callback' => 'transportable_checkout_pane_disclaimer_form',
    'title' => t('Disclaimer'),
    'desc' => t('Provides checkbox for customer to agree to transportable disclaimer.'),
    'weight' => 6,
  );
  return $panes;
}

/**
 * Checkout pane handler
*/
function transportable_checkout_pane_disclaimer_form($op) {
  $disclaimer = variable_get('transportable_checkout_disclaimer',
                _transportable_default_disclaimer());
  transportable_inc('util');
  switch ($op) {

    case 'view':
    case 'customer':
      if (!transportable_item_in_cart()) {

        return; //no item in cart
      }
      return array(
        'description' => t('In order to proceed you must agree to the following statement'),
        'contents' => array(
          'transportable_disclaimer' => array(
            '#value' => $disclaimer,
            '#weight' => -10
          ),
          'transportable_disclaimer_approval' => array(
            '#type' => 'checkboxes',
            '#title' => t('Disclaimer'),
            '#required' => TRUE,
            '#options' => array(
              'agree' => t('I agree with this statement')
            ),
            '#weight' => 10
          )
        ),
        'theme' => 'transportable_disclaimer_approval_form'
      );
      break;

    case 'process':

      break;

  }

}

/**
 * Implementation of hook_line_item().
 */
function transportable_line_item() {

}


/**
 * Implementation of hook_order().
 */
function transportable_order($op, &$arg1, $arg2) {

}


/**
 * Implementation of hook_product_types().
 */
function transportable_product_types() {
  return array('transportable_service');
}


/**
 * Implementation of hook_update_cart_item().
 */
function transportable_update_cart_item($nid, $data = array(), $qty, $cid = NULL) {
  if (!$nid) return NULL;
  $cid = !(is_null($cid) || empty($cid)) ? $cid : uc_cart_get_id();
  if ($qty < 1) {
    //give it back to the pool
    transportable_inc('util');
    foreach ($data['sids'] as $tsis_id => $seat) {
      transportable_return_seat_to_pool($tsis_id, $seat);
    }
    uc_cart_remove_item($nid, $cid, $data);
  }
  else {
    drupal_set_message(t('You cannot alter the number of tickets from the shopping cart,
                         you must remove the item and make a new enquiry using
                         the <a href="/transportable/search">search page</a>'));
  }
  uc_cart_get_contents(NULL, 'rebuild');
}


/**
 * Utility function to load includes
 * @param $name string include file name
 * eg transportable.$name.inc
*/
function transportable_inc($name) {
  module_load_include('inc', 'transportable', 'transportable.'. $name);
}

/**
 * ahah callback handler.
 *
 */
function transportable_ahah() {
  module_load_include('inc', 'node', 'node.pages');
  //get args
  $elements = func_get_args();
  //load includes
  $include = array_shift($elements);
  transportable_inc($include);
  //we set submitted as TRUE so prevent _form_builder_ie_cleanup() from triggering
  //submit on ahah that does not involve a button
  $form_state = array('storage' => NULL, 'submitted' => TRUE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  $args = $form['#parameters'];
  // biff validation (thanks ahah_helper!)
  if (!isset($_POST['op'])) {
    // For the defaults
    $form['#validate'] = NULL;
    $form['#submit'] = NULL;
    // For customly set #validate and #submit handlers.
    $form_state['submit_handlers'] = NULL;
    $form_state['validate_handlers'] = NULL;
  }

  $form_id = array_shift($args);
  $form['#post'] = $_POST;
  $form['#redirect'] = FALSE;
  $form['#programmed'] = FALSE;
  $form_state['post'] = $_POST;
  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  foreach ($elements as $element) {
    $form = $form[$element];
  }
  if (!$form) {
    watchdog('Transport', 'Ahah method was called with elements not found in resultant form', array(), WATCHDOG_ERROR);
    $form = array(
      'error' => array(
        '#value' => t('An error occured while processing your request, please try again. If the problem persists, please contact the site administrator')
      )
    );
  }
  unset($form['#prefix'], $form['#suffix']); // Prevent duplicate wrappers.
  $javascript = drupal_add_js(NULL, NULL, 'header');
  // Loop through the JS settings and find the settings needed for our buttons.
  $new_ahah_settings = array();
  if (isset($javascript['setting'])) {
    foreach ($javascript['setting'] as $settings) {
      if (isset($settings['ahah'])) {
        foreach ($settings['ahah'] as $id => $ahah_settings) {
            $new_ahah_settings[$id] = $ahah_settings;
        }
      }
    }
  }

  // Add the AHAH settings needed for our new elements.
  if (!empty($new_ahah_settings)) {
    $output = '<script type="text/javascript">jQuery.extend(Drupal.settings.ahah, '. drupal_to_js($new_ahah_settings) .');</script>';
  }

  drupal_json(array(
    'status'   => TRUE,
    'data'     => theme('status_messages') . $output . drupal_render($form),
    'settings' => call_user_func_array('array_merge_recursive', $javascript['setting'])
  ));
}

/**
 * Provides access callback for transportable service schedule nodes
 * @todo multi-user permissions
*/
function transportable_service_schedule_access($node) {
  //only transportable service nodes
  if ($node->type != 'transportable_service') {
    return FALSE;
  }
  global $user;
  return (user_access('edit own Transport Service') && $user->uid == $node->uid) ||
    user_access('edit any Transport Service');
}

/**
 * Implementation of hook_cron().
 * return tix to pool
 */
function transportable_cron() {
  transportable_inc('util');
  $now = date_make_date(date('Y-m-d h:i:s'), date_default_timezone_name(), DATE_ISO);

  //give them 10 minute grace just to be nice and in case they are stuffing around
  //with payment or similar
  $now->modify('-10 minutes');

  $res = db_query("SELECT * FROM {transportable_service_instance_seat}
                  WHERE expiry < '%s'
                  AND state = 1", $now->format(DATE_FORMAT_DATETIME));

  $cart_res = db_query("SELECT cart_item_id, cart_id, data FROM {uc_cart_products}");

  $cart_items = $rebuild_carts = array();

  while ($cart_item = db_fetch_object($cart_res)) {
    $data = unserialize($cart_item->data);
    if ($data && $data['module'] == 'transportable' &&
        is_array($data['sids'])) {
      foreach ($data['sids'] as $seat_id => $seat) {
        $cart_items[$seat_id][$seat] = array($cart_item->cart_item_id, $cart_item->cart_id);
      }
    }
  }

  while ($seat = db_fetch_object($res)) {
    //log to watchdog
    transportable_return_seat_to_pool($seat->tsis_id, $seat->seat);
    //get cart item
    $cart_item = $cart_items[$seat->tsis_id][$seat->seat];
    if (is_array($cart_item)) {
      list($cart_item_id, $cart_id) = $cart_item;
      //mark cart for rebuild
      $rebuild_carts[$cart_id] = $cart_id;
      //remove cart item
      db_query("DELETE FROM {uc_cart_products}
               WHERE cart_item_id = %d", $cart_item_id);
      watchdog('transportable', 'Removed cart item !cart_item_id from cart !cart_id,
               was seat !seat in seat instance !tsis_id, reservation expired.',
             array('!cart_item_id' => $cart_item_id,
                   '!cart_id' => $cart_id,
                   '!seat' => $seat->seat,
                   '!tsis_id' => $seat->tsis_id));
    }
    watchdog('transportable', 'Increased availability for seat !seat in seat
             instance !tsis_id, reservation expired.',
           array('!cart_item_id' => $cart_item_id,
                 '!cart_id' => $cart_id,
                 '!seat' => $seat->seat,
                 '!tsis_id' => $seat->tsis_id));
  }

  //rebuild cart contents in cache
  //NB this could very well be overkill b/c at present
  //cart contents are only cached in a static var
  foreach ($rebuild_carts as $cart_id) {
    uc_cart_get_contents($cart_id, 'rebuild');
  }
}


/**
 * Loads the transportable schedule instance
 * @param $instance_id int
 * @todo
*/
function transportable_schedule_instance_load($instance_id, $reset = FALSE) {
  static $instances;
  if (is_array($instances) && $instances[$instance_id] && !$result) { //have we cached it?
    //serve the cached one
    return $instances[$instance_id];
  }
  transportable_inc('util');
  $instance = db_fetch_object(db_query("SELECT * FROM {transportable_service_instance}
                                  WHERE id = %d", $instance_id));
  if ($instance) {
    //timezone handling
    $timezone =  date_default_timezone_name();
    $timezone_db = 'UTC';
    $instance->db_date = $instance->instance_dt;
    $instance->instance_dt = date_make_date($instance->instance_dt, $timezone_db);
    if ($timezone != $timezone_db) {
      date_timezone_set($instance->instance_dt, timezone_open($timezone));
    }
    $instance->display_dt = date_format($instance->instance_dt, variable_get('date_format_short', 'm/d/Y - H:i'));
    $instance->transportable_service = node_load($instance->ts_nid);
    $instance->stop_points = _transportable_service_get_instance_stop_points(node_load($instance->ts_nid), $instance);
    $instance->rates = _transportable_service_get_stop_point_rates($instance);
    list($instance->sales_exist, $instance->seats) = _transportable_service_get_instance_seats($instance, $instance->transportable_service->transportable_method->seats);
  }
  //cache for performance
  $instances[$instance_id] = $instance;
  return $instance;
}

/**
 * Override theme_date_popup to add more classes
*/
function theme_date_popup_transportable($element) {
  $output = '';
  $class = 'container-inline-date';
  // Add #date_float to allow date parts to float together on the same line.
  if (empty($element['#date_float'])) {
    $class .= ' date-clear-block';
  }
  if (isset($element['#attributes']) && isset($element['#attributes']['class'])) {
    $class .= ' '. $element['#attributes']['class'];
  }
  if (isset($element['#children'])) {
    $output = $element['#children'];
  }
  return '<div class="'. $class .'">'. theme('form_element', $element, $output) .'</div>';
}
