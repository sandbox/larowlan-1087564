<?php
// $Id$
/*
 * @file transportable.admin.inc
 * Provides admin screens for transportable module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 *
 */

/**
 * Form builder for transportable general settings
 *
*/
function transportable_admin_form($form_state) {
  $form = array();

  $form['bookings'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Seat reservations'),
    '#collapsed'    => FALSE,
    '#group'        => 'transportable',
    '#description' => t('Set the length of time seat bookings will remain in the cart for customers who <strong>have not</strong> completed their sale. This
                        is independent to the Ubercart anonymous cart settings. Even tickets for authenticated users will be removed from
                        their cart if they fail to finalise their sale inside this time. If the Ubercart anonymouse cart settings stipulate a shorter duration
                        then that time will be used instead.'),
  );

  transportable_inc('util');
  $durations = _transportable_durations();

  //shamelessly borrowed from uc_cart_cart_settings_form
  $form['bookings']['transportable_save_tickets_duration'] = array(
    '#type' => 'select',
    '#title' => t('Duration'),
    '#options' => drupal_map_assoc(uc_range(1, 60)),
    '#default_value' => variable_get('transportable_save_tickets_duration', '1'),
    '#prefix' => '<div style="float: left; margin-right: 1em;">',
    '#suffix' => '</div>',
  );
  $form['bookings']['transportable_save_tickets_units'] = array(
    '#type' => 'select',
    '#title' => t('Unit of time'),
    '#options' => array(
      'minutes' => t('Minute(s)'),
      'hours' => t('Hour(s)'),
      'days' => t('Day(s)'),
      'weeks' => t('Week(s)'),
      'years' => t('Year(s)'),
    ),
    '#default_value' => variable_get('transportable_save_tickets_units', 'hours'),
    '#prefix' => '<div style="float: left; margin-right: 1em;">',
    '#suffix' => '</div>',
  );

  $form['bookings']['transportable_checkout_disclaimer'] = array(
    '#type'          => 'textarea',
    '#prefix'        => '<div style="clear:left">&nbsp;</div>',
    '#title'         => t('Concession statement'),
    '#default_value' => variable_get('transportable_checkout_disclaimer',
                                     _transportable_default_disclaimer()),
    '#description'   => t('Enter the transportable disclaimer statement that customers must agree to before proceeding with checkout'),
    '#required'      => TRUE,
    '#resizable' => TRUE
  );

  $form['variables'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Variables'),
    '#collapsed'    => FALSE,
    '#group'        => 'transportable'
  );

  $form['variables']['transportable_service_condition'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Concession statement'),
    '#default_value' => variable_get('transportable_service_condition',
'Paved
Partially paved
Dusty road'),
    '#description'   => t('Enter options for service conditions, one per line. Note changing the order of
                          these values will alter existing service data. If new conditions are needed, add them
                          to the end of the list'),
    '#required'      => TRUE,
    '#resizable' => TRUE
  );



  $form['info'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Installation Information'),
    '#collapsed'    => FALSE,
    '#group'        => 'transportable'
  );

  $vtabs_status = t('is not');
  $recommend = t('Installing this module will improve the look and usability of the admin screens and node creation pages.');
  if (module_exists('vertical_tabs')) {
    $vtabs_status = t('is');
    $recommend = '';
  }
  $form['info']['vertical_tabs_info'] = array(
    '#value' => t(
      'Vertical tabs module !status installed. !recommend',
      array(
        '!status' => $vtabs_status,
        '!recommend' => $recommend
      )
    )
  );

  if (function_exists('vertical_tabs_form_pre_render')) {
    $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
  }

  return system_settings_form($form);
}


/**
 * Generates page for overview of transportable content
 * for a supplier/user
*/
function transportable_user_admin_overview($account) {
  drupal_add_css(drupal_get_path('module', 'transportable') .'/css/transportable.admin.css');
  drupal_set_title(t('My Transport services'));

  $path = drupal_get_path('module', 'transportable'). '/images/';

  $links = array();
  if (user_access('create Transport Method')) {
    $links[] = array(
      'image' => l(theme('image', $path .'transportable-method.png'), 'node/add/transportable-method', array('html' => TRUE)),
      'link' => l(t('Transport method'), 'node/add/transportable-method'),
      'help' => t('Add a new transportable method')
    );
  }
  if (user_access('create Transport Service')) {
    $links[] = array(
      'image' => l(theme('image', $path .'transportable-service.png'), 'node/add/transportable-service', array('html' => TRUE)),
      'link' => l(t('Transport service'), 'node/add/transportable-service'),
      'help' => t('Add a new transportable service')
    );
  }
  if (user_access('create Stop Points')) {
    $links[] = array(
      'image' => l(theme('image', $path .'stop-point.png'), 'node/add/stop-point', array('html' => TRUE)),
      'link' => l(t('Stop point'), 'node/add/stop-point'),
      'help' => t('Add a new stop point')
    );
  }
  if (user_access('Administer concession settings')) {
    $links[] = array(
      'image' => l(theme('image', $path .'concessions.png'), 'admin/store/settings/transportable/concessions', array('html' => TRUE)),
      'link' => l(t('Concessions'), 'admin/store/settings/transportable/concessions'),
      'help' => t('Maintain concessions')
    );
  }
  if (user_access('Administer transportable staff permissions')) {
    $links[] = array(
      'image' => l(theme('image', $path .'permissions.png'), 'admin/store/settings/transportable/permissions', array('html' => TRUE)),
      'link' => l(t('Permissions'), 'admin/store/settings/transportable/permissions'),
      'help' => t('Maintain permissions')
    );
  }

  $links = theme('transportable_admin_links', $links);
  $output = '<h3>'. t('Transport Manager') .'</h3>
    <div class="transportable-admin-links">'.
      $links
    .'</div>';

  if (user_access('create Transport Service')) {
    //services
    $output .= '<h3>'. t('Transport Services') .'</h3>';
    $output .= views_embed_view('transportable_service_list_admin', 'block_1', $user->uid);
  }

  if (user_access('create Transport Method')) {
    //methods
    $output .= '<h3>'. t('Transport Methods') .'</h3>';
    $output .= views_embed_view('transportable_method_list_admin', 'block_1', $user->uid);
  }

  if (user_access('create Stop Points')) {
    //stop points
    $output .= '<h3>'. t('Stop Points') .'</h3>';
    $output .= views_embed_view('transportable_stop_point_list_admin', 'block_1', $user->uid);
  }
  return $output;
}