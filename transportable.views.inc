<?php
// $Id$
/*
 * @file transportable.views.inc
 * Provides views integration for transportable module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 *
 */

/**
 * Implementation of hook_views_data
*/
function transportable_views_data(){
  $data = array();

  $data['transportable_service']['table']['group'] = t('Transport');
  $data['transportable_service']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
  );

  $data['transportable_service']['supplier_id'] = array(
    'title' => t('Service supplier node id'),
    'help' => t('Nid of the supplier for the transportable service'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric'
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'left_field' => 'supplier_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Supplier Node')
    )
  );

  $data['transportable_method']['table']['group'] = t('Transport');
  $data['transportable_method']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
  );

  $data['transportable_method']['supplier_id'] = array(
    'title' => t('Method supplier node id'),
    'help' => t('Nid of the supplier for the transportable method'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric'
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'left_field' => 'supplier_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Supplier Node')
    )
  );

  $data['transportable_method']['number_of_seats'] = array(
    'title' => t('Number of seats'),
    'help' => t('The number of seats on the transportable method'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field_numeric'
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric'
    )
  );

  $data['transportable_method']['free_seating'] = array(
    'title' => t('Free seating'),
    'help' => t('Whether or not there is free seating on the transportable method'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'click sortable' => TRUE,
      'handler' => 'views_handler_field_boolean'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator'
    )
  );

  $data['transportable_stop_point']['table']['group'] = t('Transport');
  $data['transportable_stop_point']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
  );

  $data['transportable_stop_point']['supplier_id'] = array(
    'title' => t('Stop point supplier node id'),
    'help' => t('Nid of the supplier for the stop point'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid'
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric'
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'left_field' => 'supplier_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Supplier Node')
    )
  );

  return $data;
}