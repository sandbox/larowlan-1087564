<?php
// $Id$
/*
 * @file transportable.theme.inc
 * Provides theme functions for transportable module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 *
 */
/**
 * Default implementation of theme_seating_plan_edit
 * @ingroup themeable
 * @param $form array form element
 * @return string html
*/
function theme_seating_plan_edit($form) {
  $output = '';
  return $output;
}

/**
 * Default implementation of theme_setting_plan_select
 * @ingroup themeable
 * @param $form array form element
 * @return string html
*/
function theme_seating_plan_select($form) {
  $output = '';
  return $output;
}

/**
 * Default implementation of theme_transportable_cart_item_detail
 * @ingroup themeable
*/
function theme_transportable_cart_item_detail($details) {
  $header = array(t('Details'), t('Departs'), t('Arrives'));
  return theme(
    'table',
    $header,
    $details
  );
}

/**
 * Default implementation of theme_f4n_checkout_pane_approval_form
 * @param $form array form element
 * @return string html
*/
function theme_transportable_disclaimer_approval_form($form) {
  $output = '';
  $output .= '<div class="transportable-approval-disclaimer">'. drupal_render($form['transportable_disclaimer']) .'</div>';
  $output .= drupal_render($form['transportable_disclaimer_approval']);
  return $output;
}

/**
 * Theme function for concession editing page.
 *
 * @ingroup themeable
 */
function theme_transportable_concession_form_items($form) {
  $rows = array();
  $headers = array(
    t('Name'),
    t('Type'),
    t('Age criteria'),
    t('Max # of seats per vehicle'),
    t('Price deduction'),
    t('transportable-admin'),
    t('Operations'),
  );

  foreach (element_children($form) as $key) {
    // Build the table row.
    $row = array(
      array('data' => drupal_render($form[$key]['title']), 'class' => 'transportable-concession-title'),
      array('data' => drupal_render($form[$key]['concession_type']), 'class' => 'transportable-concession-type'),
      array('data' => drupal_render($form[$key]['age_criteria']) . drupal_render($form[$key]['age']) , 'class' => 'transportable-concession-age'),
      array('data' => drupal_render($form[$key]['max_type']) . drupal_render($form[$key]['max_percent']) , 'class' => 'transportable-concession-max'),
      array('data' => drupal_render($form[$key]['discount_percent']), 'class' => 'transportable-concession-discount-percent'),
      array('data' => drupal_render($form[$key]['transportable-admin_id']), 'class' => 'transportable-concession-transportable-admin'),
      array('data' => drupal_render($form[$key]['remove']), 'class' => 'transportable-concession-remove'),
    );

    $rows[] = $row;
  }

  $output .= theme('table', $headers, $rows, array('id' => 'transportable-concession-items-table'));
  $output .= drupal_render($form);

  drupal_add_css(drupal_get_path('module', 'transportable') .'/css/transportable-concession.css');

  return $output;
}

/**
 * Theme function for service stop point editing table.
 *
 * @ingroup themeable
 */
function theme_transportable_service_stop_point_form_items($form) {
  drupal_add_tabledrag('transportable-service-stop-point-items-table', 'order', 'sibling', 'transportable-service-stop-point-weight-field');

  $rows = array();
  $headers = array(
    t('Name'),
    t('Weight'),
    t('Embark and Disembark'),
    t('Ticket Sales'),
    t('Operations'),
  );

  foreach (element_children($form) as $key) {
    $form[$key]['weight']['#attributes']['class'] = 'transportable-service-stop-point-weight-field';
    // Build the table row.
    $embark_cell = '<div>'. drupal_render($form[$key]['stop_point_type']) .'</div>
                    <div><div class="form-item clear width-100">'. t('Embark on') .'</div>'. drupal_render($form[$key]['embark_type']) .
                            drupal_render($form[$key]['embark_cd']) .'</div>
                    <div><div class="form-item clear width-100">'. t('Disembark on') .'</div>'. drupal_render($form[$key]['disembark_type']) .
                            drupal_render($form[$key]['disembark_cd']) .'</div>
                    <div class="clear">'. drupal_render($form[$key]['is_meal_break']) .'</div>';
    $row = array(
      'data' => array(
        array('data' => drupal_render($form[$key]['title']), 'class' => 'transportable-service-stop-point-title'),
        array('data' => drupal_render($form[$key]['weight']), 'class' => 'transportable-service-stop-point-weight'),
        array('data' => $embark_cell, 'class' => 'transportable-service-stop-point-embark'),
        array('data' => drupal_render($form[$key]['tickets_are_sold']), 'class' => 'transportable-service-stop-point-tickets-are-sold'),
        array('data' => drupal_render($form[$key]['remove']), 'class' => 'transportable-service-stop-point-remove'),
      ),
      'class' => 'draggable'
    );

    $rows[] = $row;
  }
  //if no rows
  if (!count($rows)) {
    $rows[] = array(array('data' => t('There are no stop points in this service yet'), 'colspan' => 6));
  }

  $output .= theme('table', $headers, $rows, array('id' => 'transportable-service-stop-point-items-table'));
  $output .= drupal_render($form);

  drupal_add_css(drupal_get_path('module', 'transportable') .'/css/transportable-service.css');
  drupal_add_js(drupal_get_path('module', 'transportable') .'/js/transportable-service.js');

  return $output;
}

/**
 * Default implementation of theme_transportable_search_form
*/
function theme_transportable_search_form($form) {
  $output = '';
  $path = drupal_get_path('module', 'transportable') .'/';
  drupal_add_css($path .'transportable.search.css');
  drupal_add_js($path .'transportable.search.js');
  $hide = '';
  if ($form['return']['#defalt_value'] != 1) {
    $hide = ' transportable-hidden';
  }
  $render_first = array('return', 'from', 'to', 'px', 'depart_dt', 'depart_time');
  foreach ($render_first as $field) {
    $output .= drupal_render($form[$field]);
  }
  #@todo this hide bizo not wrking
  $output .= '<div class="transportable-return-wrap'. $hide .'">'.
    drupal_render($form['return_dt']) .
    drupal_render($form['return_time'])
  .'</div>';
  $output .= drupal_render($form);
  return $output;
}

/**
 * Default implementation of theme_transportable_search_results
 * @param $form array fapi form element
 * @param $return boolean flag return trip or depart
*/
function theme_transportable_search_results($form) {
  transportable_inc('util');
  $output = '';
  if ($form['#search_return']) {
    drupal_set_message(t('Please choose your return trip'));
    $output .= '<h3 class="clear">'. t('Please choose your return trip') .'</h3>';
  }
  $header = array(
    t('Service'),
    t('Departs'),
    t('Arrives'),
    t('Rate'),
    t('Book')
  );
  $rows = array();

  //for uc_price
  $context = array(
    'revision' => 'themed',
    'type' => 'product',
    'class' => array('product', 'sell'),
  );

  $records = element_children($form['rates']['direct']);
  if (count($records) == 0) {
    $rows[] = array(array('data' => t('There were no direct transportable services matching your search, please change your parameters and try again'), 'colspan' => '5'));
    $no_results = TRUE;
  }
  else {
    $embark_types = _transportable_embark_type();
    $service_conditions = _transportable_service_condition();
    foreach ($records as $price_id) {
      $details = $form['rates']['direct'][$price_id]['#value'];
      list($service, $method, $reserved, $depart, $arrive, $from, $to) = _transportable_search_result_details($details, $service_conditions, $embark_types);

      $row = array(
        '<div class="transportable-service-name">'. l($service->title, drupal_get_path_alias('node/'. $service->nid)) .'</div>
        <div class="transportable-method-details">'. check_plain($method->title) . t(
          ' (!seats seats !reserved)',
          array(
            '!seats' => $method->number_of_seats,
            '!reserved' => $reserved
          )
        ) .'</div>',
        '<div class="transportable-service-depart">'. l($from->title, drupal_get_path_alias('node/'. $from->nid)) .'</div>
        <div class="transportable-service-depart-detail">'. $depart .'</div>',
        '<div class="transportable-service-arrive">'. l($to->title, drupal_get_path_alias('node/'. $to->nid)) .'</div>
        <div class="transportable-service-arrive-detail">'. $arrive .'</div>',
        array('data' => uc_price($details->rate, $context, array('label' => FALSE)), 'class' => 'transportable-price'),
        drupal_render($form['book']['direct'][$price_id])
      );
      $rows[] = $row;
    }
  }
  $output .= theme('table', $header, $rows, array('class' => 'transportable-search-results-table'));
  if ($no_results) {
    $rows = array();
    $records = element_children($form['rates']['indirect']);
    if (count($records) ==  0) {
      $rows[] = array(array('data' => t('There were no indirect transportable services matching your search, please change your parameters and try again'), 'colspan' => '5'));
      $output .= theme('table', $header, $rows, array('class' => 'transportable-search-results-table'));
    }
    else {
      $output .= '<h3 class="clear">'. t('In-direct matches for your criteria') .'</h2>';
      $embark_types = _transportable_embark_type();
      $service_conditions = _transportable_service_condition();
      array_pop($header); //no book column
      foreach ($records as $delta) {
        $rows[] = array();
        foreach (element_children($form['rates']['indirect'][$delta]) as $price_id) {
          $details = $form['rates']['indirect'][$delta][$price_id]['#value'];
          list($service, $method, $reserved, $depart, $arrive, $from, $to) = _transportable_search_result_details($details, $service_conditions, $embark_types);

          $row = array(
            '<div class="transportable-service-name">'. l($service->title, drupal_get_path_alias('node/'. $service->nid)) .'</div>
            <div class="transportable-method-details">'. check_plain($method->title) . t(
              ' (!seats seats !reserved)',
              array(
                '!seats' => $method->number_of_seats,
                '!reserved' => $reserved
              )
            ) .'</div>',
            '<div class="transportable-service-depart">'. l($from->title, drupal_get_path_alias('node/'. $from->nid)) .'</div>
            <div class="transportable-service-depart-detail">'. $depart .'</div>',
            '<div class="transportable-service-arrive">'. l($to->title, drupal_get_path_alias('node/'. $to->nid)) .'</div>
            <div class="transportable-service-arrive-detail">'. $arrive .'</div>',
            array('data' => uc_price($details->rate, $context, array('label' => FALSE)), 'class' => 'transportable-price')
          );
          $rows[] = $row;
        }
        $rows[] = array(array('data' => drupal_render($form['book']['indirect'][$delta]), 'colspan' => 4, 'class' => 'transportable-search-indirect-book'));

        $output .= theme('table', $header, $rows, array('class' => 'transportable-search-results-table-indirect'));
      }
    }
  }
  $output .= drupal_render($form);
  return $output;
}

/**
 * Default implementation of theme_transportable_book_form
*/
function theme_transportable_book_form($form) {
  $output = '';
  $output .= drupal_render($form);
  return $output;
}

/**
 * Default implementation of theme_transportable_offline_sales_table
*/
function theme_transportable_offline_sales_table($form) {
  $output = '';
  $header = array(t('To'), t('Available'), t('Offline sales'));
  $rows = array();
  foreach (element_children($form) as $key) {
    $element = $form[$key];
    $row = array(
      drupal_render($element['caption']),
      drupal_render($element['avail']),
      drupal_render($element['sales'])
    );
    $rows[] = $row;
  }
  if (count($rows) == 0) {
    $rows[] = array(array('data' => t('There are no matching stop points to sell tickets to for this instance and stop point'), 'colspan' => '3'));
  }
  $output .= theme('table', $header, $rows, array('class' => 'transportable-offline-sales-table'));
  return $output;
}

/**
 * Default implementation of theme_transportable_admin_links
*/
function theme_transportable_admin_links($links) {
  $output = "";
  $switch = ' alpha';
  foreach ($links as $link) {
    $output .= '
    <div class="transportable-admin-link'. $switch .'">
      <div class="transportable-admin-link-image">'. $link['image'] .'</div>
      <div class="transportable-admin-link-text">
        <div class="transportable-admin-link-link">'. $link['link'] .'</div>
        <div class="transportable-admin-link-help">'. $link['help'] .'</div>
      </div>
    </div>';
    $switch = ($switch == ' alpha' ? ' omega' : ' alpha');
  }
  $output .= '<div class="clear">&nbsp;</div>';
  return $output;

}