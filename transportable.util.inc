<?php
// $Id$
/*
 * @file transportable.util.inc
 * Provides utility functions for transportable module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html 
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 *
 * note many util functions return array of strings, providing meanings
 * for integer codes, for ease of readability and quick reference, these arrays
 * are written with their keys, but because they are zero-based numeric integer keys,
 * these could easily have been written as flat, keyless arrays with the keys auto-assigned
 * 
 */

/**
 * Provides array of transportable seat types
 * 
*/
function _transportable_seat_types() {
  return array(
    0 => t('Non reclining'),
    1 => t('Reclining (semi-bed'),
    2 => t('Reclining (bed)')
  );
}

/**
 * Provides details of concession types
*/
function _transportable_concession_types() {
  return array(
    0 => t('Children'),
    1 => t('Student'),
    2 => t('Retiree'),
    3 => t('Pensioner')
  );
}

/**
 * Provides details of concession age criteria
*/
function _transportable_concession_age_criteria() {
  return array(
    0 => t('No age criteria'),
    1 => t('At least'),
    2 => t('No older than')
  );
}

/**
 * Provides details of basis for determining maximum # of concessions
*/
function _transportable_concession_max_type() {
  return array(
    0 => t('Consider all concessions'),
    1 => t('This concession alone')
  );
}

/**
 * Provides details of disembark/embark states for
 * stop point
*/
function _transportable_stop_point_type() {
  return array(
    0 => t('Both'),
    1 => t('Embark only'),
    2 => t('Disembark only')
  );
}

/**
 * Provides type of stop point embark/disembark at
*/
function _transportable_embark_type() {
  return array(
    0 => t('N/A'),
    1 => t('Platform'),
    2 => t('Gate'),
    3 => t('Bay'),
    4 => t('Bus stop'),
  );
}

/**
 * Provides service conditions ahead
*/
function _transportable_service_condition() {
  $conditions = variable_get('transportable_service_condition',
'Paved
Partially paved
Dusty road');
  $conditions = explode("\n", $conditions);//1 item per line
  $conditions = array_map('trim', $conditions);//get rid of white space
  $conditions = array_filter($conditions, 'strlen');//get rid of zero length lines
  array_unshift($conditions, t('Not applicable')); //have to translate this b/c
  //other vars are translated as variables @see transportable_init
  return $conditions;
}

/**
 * Provides details of cart item for display in cart
 * @param $context array context variable suitable for uc_price
 * @param $item object cart item
 * @return array ('total' => price, 'details' => array of rows for table)
*/
function _transportable_cart_item_details($context, $item) {
  $data = $item->data;
  $detail = $data['details'];
  $embark_types = _transportable_embark_type();
  $service_conditions = _transportable_service_condition();
  list($service, $method, $reserved, $depart, $arrive, $from, $to) = _transportable_search_result_details($detail, $service_conditions, $embark_types);
  $row = array(
    '<div class="transportable-method-details">'. check_plain($method->title) . t(
      ' (!seats seats !reserved)',
      array(
        '!seats' => $method->number_of_seats,
        '!reserved' => $reserved
      )
    ) .'</div>',
    '<div class="transportable-service-depart">'. l($from->title, drupal_get_path_alias('node/'. $from->nid)) .'</div>
    <div class="transportable-service-depart-detail">'. $depart .'</div>',
    '<div class="transportable-service-arrive">'. l($to->title, drupal_get_path_alias('node/'. $to->nid)) .'</div>
    <div class="transportable-service-arrive-detail">'. $arrive .'</div>'
  );
  $price = $detail->rate;
  
  $cid = $service->concessions[$data['concession']];
  
  if ($cid) {
    $concession = db_fetch_object(db_query("SELECT *
                                           FROM {transportable_concessions}
                                           WHERE cid = %d", $cid));
    if (is_object($concession) && $concession->discount_percent) {
      $price = $price * (1 - ($concession->discount_percent)/100);
    }
  }
  
  $return = array('total' => $price, 'details' => array($row));
  return $return;
}

/**
 * Helper function to determine if there is a booking item in the cart.
 */
function transportable_item_in_cart() {
  $items = uc_cart_get_contents();
  foreach ($items as $item) {
    if ($item->data['module'] == 'transportable') {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Helper function to determine if concessions in the cart
*/
function transportable_concession_item_in_cart() {
  $items = uc_cart_get_contents();
  foreach ($items as $item) {
    if ($item->data['module'] == 'transportable') {
      if (is_array($item->data['tickets'])) {
        foreach ($item->data['tickets'] as $ticket) {
          if ($ticket->concession == TRUE) {
            return TRUE;
          }
        }
      }
    }
  }
  return FALSE;
}

/**
 * Utility function to return $durations array
*/
function _transportable_durations() {
  //shamelessly borrowed from uc_cart_cart_settings_form
  return array(
    'singular' => array(
      'minutes' => t('minute'),
      'hours' => t('hour'),
      'days' => t('day'),
      'weeks' => t('week'),
      'years' => t('year'),
    ),
    'plural' => array(
      'minutes' => t('minutes'),
      'hours' => t('hours'),
      'days' => t('days'),
      'weeks' => t('weeks'),
      'years' => t('years'),
    ),
  );
}

/**
 * Utility function to return default disclaimer
*/
function _transportable_default_disclaimer() {
  return 'I acknowledge that I posess the required documentation to
        validate my claim to one or more concession tickets. I acknowledge that failure
        to produce such documentation may result in additional charges or a refusal to honour my booking on behalf of
        the transportable provider/carrier. I acknowledge that departure and arrival times are indicative only and that
        the transportable provider/carrier may alter or cancel services from time to time without notice.';
}


/**
 * Provides list of transportable method nodes
 * @return array nid -> title
*/
function _transportable_get_transportable_methods() {
  $suppliers = array_keys(travel_supplier_get_suppliers());
  $res = db_query("SELECT n.nid, n.title
                  FROM {node} n, {transportable_method} tm
                  WHERE tm.vid = n.vid
                  AND tm.supplier_id IN (". db_placeholders($suppliers) .")",
                  $suppliers);
  $methods = array();
  while ($method = db_fetch_array($res)) {
    $methods[$method['nid']] = check_plain($method['title']);
  }
  return $methods;
}

/**
 * Provides list of transportable stop points (not instances)
 * @return array nid->title map
*/
function _transportable_get_stop_points() {
  $suppliers = array_keys(travel_supplier_get_suppliers());
  //add all suppliers
  $suppliers[] = 0;
  global $user;
  $res = db_query("SELECT n.nid, n.title
                  FROM {node} n, {transportable_stop_point} tsp
                  WHERE tsp.vid = n.vid
                  AND tsp.supplier_id IN (". db_placeholders($suppliers) .") 
                  AND n.status = 1
                  ", $suppliers);
  $points = array();
  while ($point = db_fetch_array($res)) {
    if ($user->uid != 1 &&
        !$user->transportable_permissions->permissions['general']['Administer stop points'] &&
        !$user->transportable_permissions->permissions['stop_points'][$point['nid']]) {
      continue; //access denied
    }
    $points[$point['nid']] = check_plain($point['title']);
  }
  return $points;
}

/**
 * Fetch array of instances for a service
 * @param $node object transportable service node object
 * @return array of schedule records from transportable_service_instance table
*/
function _transportable_service_get_schedule($node) {
  $schedule = array();
  if ($node->type == 'transportable_service') {
    $res = db_query("SELECT * FROM {transportable_service_instance}
                    WHERE ts_nid = %d
                    ORDER BY instance_set_id, delta", $node->nid);
    while ($instance = db_fetch_array($res)) {
      $instance['repeat_desc'] = date_repeat_rrule_description($instance['repeat_rule']);
      $instance['instance_dt_display'] = _transportable_date_to_timezone($instance['instance_dt'], variable_get('date_format_short', 'm/d/Y - H:i'));
      $schedule[] = $instance;
    }
  }
  return $schedule;
}

/**
 * Generate a new transportable service instance set id
 * @param $node object node object
 * @return int new instance set id
*/
function _transportable_service_instance_new_id($node) {
  $max = db_result(db_query("SELECT MAX(instance_set_id)
                            FROM {transportable_service_instance}
                            WHERE ts_nid = %d", $node->nid));
  if (!$max) {
    return 1;
  }
  $max++;
  return $max;
}

/**
 * Return an array of stop point instances for given service and schdeule instance
 * @param $node object node object
 * @param $instance object schedule instance object
 * @return array of stop point instances
*/
function _transportable_service_get_instance_stop_points($node, $instance) {
  $stop_point_instances = array();
  global $user;
  $res = db_query("SELECT tsspi.*, n.title
                  FROM {transportable_service_stop_point_instance} tsspi INNER JOIN
                  {transportable_service_stop_point} tssp ON tssp.id = tsspi.tssp_id INNER JOIN
                  {node} n ON tssp.sp_nid = n.nid
                  WHERE tsspi.tsi_id = %d", $instance->id);
  while ($stop_point_instance = db_fetch_object($res)) {
    if ($user->uid != 1 &&
        !$user->transportable_permissions->permissions['general']['Administer stop points'] &&
        !$user->transportable_permissions->permissions['stop_points'][$stop_point_instance->sp_nid]) {
      continue; //access denied
    }
    $stop_point_instance->title = check_plain($stop_point_instance->title);
    //change the depart/arrive dates from UTC to site timezone
    $stop_point_instance->depart = _transportable_date_to_timezone($stop_point_instance->depart);
    $stop_point_instance->arrive = _transportable_date_to_timezone($stop_point_instance->arrive);
    //key stop points by their transportable_service_stop_point.id FK
    $stop_point_instances[$stop_point_instance->tssp_id] = $stop_point_instance;
  }
  return $stop_point_instances;
}

/**
 * Return an array of stop points valid for a given service 
 * @param $node object/int node object for service or nid
 * @return array of stop point instances
*/
function _transportable_service_get_stop_points($node) {
  $stop_points = array();
  global $user;
  if (is_numeric($node)) {
    $nid = $node;
  }
  else {
    $nid = $node->nid;
  }
  $res = db_query("SELECT tssp.*, n.title
                  FROM {transportable_service_stop_point} tssp
                  INNER JOIN {node} n ON tssp.sp_nid = n.nid
                  WHERE ts_nid = %d ORDER BY tssp.weight, tssp.id", $nid);
  while ($point = db_fetch_array($res)) {
    if ($user->uid != 1 &&
        !$user->transportable_permissions->permissions['general']['Administer stop points'] &&
        !$user->transportable_permissions->permissions['stop_points'][$point['sp_nid']]) {
      continue; //access denied
    }
    $point['title'] = check_plain($point['title']);
    $stop_points[] = $point;
  }
  return $stop_points;
}

/**
 * Converts a datetime in the db (timezone = UTC)
 * To a datetime in the default timezone of the site
 * @param $date string date in iso format
 * @param $format string date format string
 * @return $string date 
*/
function _transportable_date_to_timezone($date, $format = DATE_FORMAT_DATETIME) {
  if (!$date) {
    return NULL;
  }
  $timezone =  date_default_timezone_name();
  $timezone_db = 'UTC';
  $date = date_make_date($date, $timezone_db);
  if ($timezone != $timezone_db) {
    date_timezone_set($date, timezone_open($timezone));
  }
  return date_format($date, $format);
}

/**
 * Converts a datetime in the user timezone to db timezone (UTC)
 * To a datetime in the default timezone of the site
 * @param $date string date in iso format
 * @param $format string date format string
 * @return $string date 
*/
function _transportable_date_to_timezone_db($date, $format = DATE_FORMAT_DATETIME) {
  if (!$date) {
    return NULL;
  }
  $timezone =  date_default_timezone_name();
  $timezone_db = 'UTC';
  $date = date_make_date($date, $timezone);
  if ($timezone != $timezone_db) {
    date_timezone_set($date, timezone_open($timezone_db));
  }
  return date_format($date, $format);
}

/**
 * Return a multidimensional array keyed by from and to fields for each
 * rate for a given instance
 * @param $instance object service instance object
 * @return array('tssp_id_from' => array('tssp_id_to' => row from table))
*/
function _transportable_service_get_stop_point_rates($instance) {
  $rates = array();
  $res = db_query("SELECT * FROM
                  {transportable_service_stop_point_rate}
                  WHERE tsi_id = %d", $instance->id);
  while ($rate = db_fetch_object($res)) {
    $rates[$rate->tssp_id_from][$rate->tssp_id_to] = $rate;
  }
  return $rates;
}

/**
 * Provides list of seat states
*/
function _transportable_seat_states() {
  return array(
    0 => t('Available'),
    1 => t('Provisional'),
    2 => t('Sold')
  );
}

/**
 * Fetch seating sales for an instance
 * @param $instance object instance object
 * @param $available_seats int seats avail on transportable method for this service
 * @return array of seating objects
*/
function _transportable_service_get_instance_seats($instance, $available_seats) {
  $seats = array();
  $res = db_query("SELECT * FROM {transportable_service_instance_seats}
                  WHERE tsi_id = %d", $instance->id);
  $sales_exist = FALSE;
  while ($seat = db_fetch_object($res)) {
    $seats[] = $seat;
    $sales_exist = ($sales_exist || ($seat->available < $available_seats));
  }
  return array($sales_exist, $seats);
}

/**
 * Fetch all instances in set
 * @param $instance_set_id int instance set id
 * @return array (max_delta => , instances => array of instance objects)
*/
function _transportable_service_instances_in_set($instance_set_id) {
  $instances = array();
  $res = db_query("SELECT * FROM {transportable_service_instance}
                  WHERE instance_set_id = %d", $instance_set_id);
  $max_delta = 0;
  while ($instance = db_fetch_object($res)) {
    $instances[] = $instance;
    $max_delta = max($instance->delta, $max_delta);
  }
  return array($max_delta, $instances);
}

/**
 * Util to provide options vals for time field in search results
 *
 * Indexes of options are added to date to make to values
 * eg if date = 2010-01-01 then you split index on ; to get time range to add to date
 * get datetime values eg use 'Anytime' to give range of 2010-01-01 00:00:00 to 2010-01-01 23:59:59
*/
function _transportable_time_periods() {
  return array(
    '00:00:00;23:59:59' => t('Anytime'),
    '00:00:00;11:59:59' => t('Morning'),
    '12:00:00;17:59:59' => t('Afternoon'),
    '18:00:00;23:59:59' => t('Evening'),
  );
}

/**
 * Utility to parse dates into a range from time period select field
 * @param $date string date/datetime if time found, it will be stripped
 * @param $period_value string key from @see _transportable_time_periods
 * return array(start_dt, end_dt)
*/
function _transportable_time_periods_to_range($date, $period_value) {
  list ($date, $time) = explode(" ", $date);
  list ($start, $end) = explode(";", $period_value);
  return array($date .' '. $start, $date .' '. $end);
}

/**
 * Utility to fetch destination points
 * @return array of the location values etc
*/
function _transportable_stop_point_list() {
  $res = db_query("SELECT DISTINCT l.city, l.province, l.country
            FROM {location} l INNER JOIN
            {location_instance} li ON li.lid = l.lid
            INNER JOIN {node} n ON li.nid = n.nid
            WHERE n.type = 'stop_point'
            AND l.city IS NOT NULL
            AND l.province IS NOT NULL
            ORDER BY 2, 1");
  $options = array();
  while ($loc = db_fetch_object($res)) {
    if (!$loc->city || !$loc->province) {
      continue;//deal with zero length strings
    }
    $province = location_province_name($loc->country, $loc->province);
    if (!$options[$province]) {
      $options[$province] = array();
    }
    //we concat location and city using !! incase two cities w/ same name in different locations
    $options[$province][$loc->province .'!!'. $loc->city] = '-'. $loc->city;
  }
  return $options;
}

/**
 * Utility to split city and province from select field
 * @param $city_province string city/location string from key of @see _transportable_stop_point_list
 * @return array (province, city)
*/
function _transportable_stop_point_split($city_province) {
  return explode("!!", $city_province);
}

/**
 * Util to fetch stop point ids (nids of stop point nodes) matching city and province
 * @param $province string province code
 * @param $city string city
 * @return array of nids
*/
function _transportable_get_location_stop_points($province, $city) {
  $points = array();
  $res = db_query("SELECT li.nid
            FROM {location} l INNER JOIN
            {location_instance} li ON li.lid = l.lid
            INNER JOIN {node} n ON li.nid = n.nid
            WHERE n.type = 'stop_point'
            AND l.city = '%s'
            AND l.province = '%s'", $city, $province);
  while ($point = db_fetch_array($res)) {
    $points[] = $point['nid'];
  }
  return $points;
}

/**
 * Utility to check if tix are avail on instance b/w two points
 * @param $service_nid int nid of service
 * @param $instance_id int id of service schedule instance
 * @param $from_sp_nid int nid of from stop point
 * @param $to_sp_nid int nid of to stop point
 * @param $tickets int number of tix required
 * @param $return_seats bool TRUE to return seats, FALSE to return boolean
 * @return bool true/false or array depending on type
*/
function _transportable_seats_are_available($service_nid, $instance_id, $from_sp_nid, $to_sp_nid, $tickets, $return_seats = FALSE) {
  $points = _transportable_service_get_stop_points($service_nid);
  $seats = _transportable_seats_in_instance($instance_id);
  $started = FALSE;
  $seat_instances = array();
  //start travelling the service in order
  foreach ($points as $point) {
    if (!$started && $point['sp_nid'] == $from_sp_nid) {
      //we've reached the from point on this service
      $started = TRUE;
    }
    if (!$started) {
      continue; //we are still travelling on this service towards the from point
    }
    if ($point['sp_nid'] == $to_sp_nid) {
      //we've made it to destination
      break;
    }
    if ($seats[$point['sp_nid']]['available'] < $tickets) {
      //not enough tix on this leg
      return FALSE;
    }
    $seat_instances[$seats[$point['sp_nid']]['id']] = $seats[$point['sp_nid']];
  }
  //made it to destination with enough tix
  return $return_seats ? $seat_instances : TRUE;
}

/**
 * Utility to fetch seats for an instance
 * @param $instance_id int instance id
 * @return array of seats keyed by from stop point nid
*/
function _transportable_seats_in_instance($instance_id) {
  $seats = array();
  $res = db_query("SELECT tsis.*,
                  sp_fr.sp_nid AS from_sp,
                  sp_to.sp_nid AS to_sp
                  FROM
                  {transportable_service_instance_seats} tsis
                  INNER JOIN {transportable_service_stop_point} sp_fr
                  ON tsis.tssp_id_from = sp_fr.id
                  INNER JOIN {transportable_service_stop_point} sp_to
                  ON tsis.tssp_id_to = sp_to.id  
                  WHERE tsi_id = %d", $instance_id);
  while ($seat = db_fetch_array($res)) {
    $seats[$seat['from_sp']] = $seat;
  }
  return $seats;
}

/**
 * Utility to flesh out values in a search result record
 * @param $details object search result details, @see transportable_search_results
 * @param $service_conditions array @see _transportable_service_condition
 * @param $embark_types array @see _transportable_embark_type
 * @return array (service, method, reserved, depart, arrive, from, to)
*/
function _transportable_search_result_details($details, $service_conditions = FALSE, $embark_types = FALSE) {
  if (!$service_conditions) {
    $service_conditions = _transportable_service_condition();
  }
  if (!$embark_types) {
    $embark_types = _transportable_embark_type();
  }
  $service = node_load($details->ts_nid);
  $method = $service->transportable_method;
  $reserved = ($method->free_seating ? '' : t(', reserved seating'));
  
  $depart = _transportable_date_to_timezone($details->depart, 'j M Y g:i a');
  $arrive = _transportable_date_to_timezone($details->arrive, 'j M Y g:i a');
  
  $from = node_load($details->sp_from);
  $to = node_load($details->sp_to);      
  
  if ($details->embark_type) {
    $depart .= t(' from !embark_type @embark_cd',
                        array('!embark_type' => $embark_types[$details->embark_type],
                              '@embark_cd' => $details->embark_cd));
  }
  
  if ($details->service_condition) {
    $depart .= '<div>'. t('Service conditions: !condition',
                          array('!condition' => $service_conditions[$details->service_condition]))
              .'</div>';
  }
  
  if ($details->disembark_type) {
    $arrive .= t(' from !embark_type @embark_cd',
                        array('!embark_type' => $embark_types[$details->disembark_type],
                              '@embark_cd' => $details->disembark_cd));
  }
  return array($service, $method, $reserved, $depart, $arrive, $from, $to);
}

/**
 * Check all transportable methods in the results to
 * see if any have reserved seating
 * @param $details array of objects as returned in @see transportable_search_results
 * @return boolean 
*/
function _transportable_search_results_feature_reserved_seating($details) {
  $ts_nids = array();
  foreach ($details as $detail) {
    $ts_nids[] = $detail->ts_nid;
  }
  if (count($ts_nids) == 0) {
    return FALSE;
  }
  $res = db_result(db_query("SELECT COUNT(*) AS reserved
                  FROM {transportable_service} ts
                  INNER JOIN {transportable_method} tm
                  ON ts.tm_nid = ts.id
                  INNER JOIN {node} n
                  ON n.vid = ts.vid
                  INNER JOIN {node} n2
                  ON n2.vid = tm.vid
                  AND ts.nid IN (". db_placeholders($ts_nids) .")
                  AND tm.free_seating = 0", $ts_nids));
  return $res;
}

/**
 * Fetch concessions for a service node
 * @param $node object node obj for transportable service
 * @return array of concession types keyed type => cid
*/
function _transportable_service_get_concessions($node) {
  $concessions = array();
  $res = db_query("SELECT concession_type, cid
                  FROM {transportable_service_concessions}
                  WHERE ts_nid = %d", $node->nid);
  
  while ($concession = db_fetch_array($res)) {
    $concessions[$concession['concession_type']] = $concession['cid'];
  }
  
  return $concessions;
}

/**
 * gets next available seat for a seat instance
 * @param $seat array, record from transportable_service_instance_seats as array
 * @return int, the next available seat
*/
function _transportable_seat_instance_next_seat($seat) {
  return db_result(db_query("SELECT MIN(seat)
                            FROM {transportable_service_instance_seat}
                            WHERE tsis_id = %d
                            AND state = 0", $seat['id']));
}

/**
 * Remove provisional booking
 * @param $seats_id int seats id (transportable_service_instance_seats)
 * @param $seat int seat #
*/
function transportable_return_seat_to_pool($seats_id, $seat) {
  //increase available, decrease provisional for seat summary
  db_query("UPDATE {transportable_service_instance_seats}
           SET available = available + 1,
           provisional = provisional - 1
           WHERE id = %d", $seats_id);
  //remove expiry, cid against seat instance
  db_query("UPDATE {transportable_service_instance_seat}
           SET state = %d,
           cid = NULL,
           expiry = NULL
           WHERE tsis_id = %d
           AND seat = %d",
           0,
           $seats_id,
           $seat);
}

/**
 * Change provisional booking to confirmed
 * @param $seats_id int seats id (transportable_service_instance_seats)
 * @param $seat int seat #
*/
function transportable_seat_sold($seats_id, $seat) {
  //increase available, decrease provisional for seat summary
  db_query("UPDATE {transportable_service_instance_seats}
           SET sold = sold + 1,
           provisional = provisional - 1
           WHERE id = %d", $seats_id);
  //remove expiry, cid against seat instance
  db_query("UPDATE {transportable_service_instance_seat}
           SET state = 2,
           WHERE tsis_id = %d
           AND seat = %d",
           $seats_id,
           $seat);
}

/**
 * Reserve a seat on cart-add/offline sales
 * @param $details obj reservation details
 * @param $concession_type
 * @param $seat int seat number, optional
 * @param $state int seat state @see _transportable_seat_states()
 * @return seat id, id of transportable_service_instance_seat
*/
function transportable_search_reserve_seat($details, $concession_type, $seat = FALSE, $state = 1) {
  //for what seat was just reserved?
  $returns = array();
  transportable_inc('util');
  if (!($seats = _transportable_seats_are_available(
        $details->ts_nid,
        $details->tsi_id,
        $details->sp_from,
        $details->sp_to,
        1, TRUE))) {
    //no tickets
    return FALSE;
  }
  $seat_instance_ids = array_keys($seats);
  //check for reserved seats
  if ($seat) {
    $not_available = db_result(db_query("SELECT COUNT(*)
                                        FROM {transportable_service_instance_seat} ts
                                        WHERE ts.state <> 0
                                        AND tsis_id IN (". db_placeholders($seat_instance_ids) .")
                                        AND seat = %d",
                                        array_merge($seat_instance_ids, array($seat))));
    if ($not_available) {
      //someone else has snagged the seat first
      return FALSE;
    }
  }
  $service = node_load($details->ts_nid);
  foreach ($seats as $reserve) {
    $this_seat = $seat;
    if (!$seat) {
      $this_seat = _transportable_seat_instance_next_seat($reserve);
      if (!$this_seat) {
        //someone else got it since the last check
        return FALSE;
      }
    }
    $returns[$reserve['id']] = $this_seat;
    $now = date_make_date(date('Y-m-d h:i:s'), date_default_timezone_name(), DATE_ISO);
    $duration = variable_get('transportable_save_tickets_duration', '1');
    $units = variable_get('transportable_save_tickets_units', 'hours');
    
    $now->modify('+ '. $duration .' '. $units);
    
    $fields = array(
      1 => 'provisional',
      2 => 'sold'
    );
    if (!in_array($state, array_keys($fields))) {
      return FALSE;
    }
    //reduce available, increase provisional for seat summary
    db_query("UPDATE {transportable_service_instance_seats}
             SET available = available - 1,
             %s = %s + 1
             WHERE id = %d", $fields[$state],
             $fields[$state], $reserve['id']);
    //record expiry and cid rate against seat instance
    $query = "UPDATE {transportable_service_instance_seat}
             SET state = %d,
             cid = %d";
    $params = array($state,
                    $service->concessions[$concession_type]);
    if ($state == 1) {
      $query .= ",
                expiry = '%s'";
      $params[] = $now->format(DATE_FORMAT_DATETIME);
    }
    $query .= "
            WHERE tsis_id = %d
             AND seat = %d";
    $params = array_merge($params, array($reserve['id'], $this_seat));
    db_query($query, $params);
  }
  return $returns;
}

/**
 * Returns users related to this user via supplier nodes
 * @return array of drupal user objects
*/
function _transportable_coworkers() {
  $suppliers = array_keys(travel_supplier_get_suppliers());
  $res = db_query("SELECT DISTINCT field_allowed_users_uid AS uid
                  FROM {content_field_allowed_users} cfau
                  INNER JOIN {node} n ON cfau.vid = n.vid
                  WHERE cfau.nid IN (". db_placeholders($suppliers) .")", $suppliers);
  $coworkers = array();
  while ($worker = db_fetch_array($res)) {
    $coworkers[$worker['uid']] = user_load($worker['uid']);
  }
  return $coworkers;
}

/**
 * Get array of sold seats for a route instance
 * @param $details array of objects as returned in @see transportable_search_results
 * @return array of available seats 
*/
function _transportable_service_instance_fetch_sold($route) {
  $seats = _transportable_seats_are_available(
        $route->ts_nid,
        $route->tsi_id,
        $route->sp_from,
        $route->sp_to,
        0, TRUE);
  $tsis_ids = array_keys($seats);
  $sold = array();
  $res = db_query("SELECT DISTINCT seat FROM
                  {transportable_service_instance_seat}
                  WHERE tsis_id IN (". db_placeholders($tsis_ids) .")
                  AND state IN (1,2)", $tsis_ids);
  while ($seat = db_fetch_array($res)) {
    $sold[] = $seat['seat'];
  }
  return $sold;
}