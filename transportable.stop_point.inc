<?php
// $Id$
/*
 * @file transportable.transportable_method.inc
 * Provides node hooks for transportable_method node type
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html 
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
 * Hook form handler
*/
function transportable_node_stop_point_form(&$form, &$node, $form_state) {
  //add to the form array
  global $user;
  $suppliers = travel_supplier_get_suppliers();
  if (user_access('Create universal stop points')) {
    $suppliers[0] = t('All suppliers');
  }
  
  //vertical tab for location
  $form['location'] = array(
    '#type'         => 'fieldset',
    '#collapsible'  => TRUE,
    '#title'        => t('Location'),
    '#collapsed'    => FALSE,
    '#weight'       => -10,
    '#group'        => 'transportable',
  );
  
  $form['basic']['supplier_id'] = array(
    '#type' => 'select',
    '#title' => t('Supplier'),
    '#default_value' => $node->supplier_id,
    '#options' => $suppliers,
    '#description' => t('Choose the supplier for this stop point'),
  );
  
}

/**
 * Hook insert handler
*/
function transportable_node_stop_point_insert($node) {
  //insert into db
  $result = drupal_write_record('transportable_stop_point', $node);
}

/**
 * Hook update handler
*/
function transportable_node_stop_point_update($node) {
  //do the update
  $update = array('vid');
  $result = drupal_write_record('transportable_stop_point', $node, $update);
}

/**
 * Hook delete handler
*/
function transportable_node_stop_point_delete(&$node) {
  //do the delete
  db_query("DELETE FROM {transportable_stop_point} WHERE vid = %d", $node->vid);
}

/**
 * Hook load handler
*/
function transportable_node_stop_point_load($node) {
  $data = db_fetch_object(db_query("SELECT * FROM {transportable_stop_point}
                                   WHERE vid = %d", $node->vid));
  return $data;
}

/**
 * Hook view handler
*/
function transportable_node_stop_point_view(&$node, $teaser, $page) {
  //set up content
}