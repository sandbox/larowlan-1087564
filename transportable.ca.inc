<?php
// $Id$
/*
 * @file transportable.ca.inc
 * Provides conditional actions for transportable module
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html 
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
* Implementation of hook_ca_predicate().
*/
function transportable_ca_predicate() {
  // Set the order status to "Payment Received" when a payment is received
  // and the balance is less than or equal to 0.
  //uc_checkout_complete
  $configurations['transportable_payment_received'] = array(
    '#title' => t('Update seat availability status on full payment'),
    '#description' => t('Only happens when a payment is entered and the balance is <= $0.00.'),
    '#class' => 'payment',
    '#trigger' => 'uc_payment_entered',
    '#status' => 1,
    '#conditions' => array(
      '#operator' => 'AND',
      '#conditions' => array(
        array(
          '#name' => 'uc_payment_condition_order_balance',
          '#title' => t('If the balance is less than or equal to $0.00.'),
          '#argument_map' => array(
            'order' => 'order',
          ),
          '#settings' => array(
            'negate' => FALSE,
            'balance_comparison' => 'less_equal',
          ),
        ),
        array(
          '#name' => 'uc_order_status_condition',
          '#title' => t('If the order status is not already Payment Received.'),
          '#argument_map' => array(
            'order' => 'order',
          ),
          '#settings' => array(
            'negate' => TRUE,
            'order_status' => 'payment_received',
          ),
        ),
      ),
    ),
    '#actions' => array(
      array(
        '#name' => 'transportable_confirm_reservations',
        '#title' => t('Mark seat reservations as confirmed.'),
        '#argument_map' => array(
          'order' => 'order',
        ),
        '#settings' => array(
          'order_status' => 'payment_received',
        ),
      ),
    ),
  );

  return $configurations;
}

/**
 * Implementation of hook_ca_action
*/
function transportable_ca_action() {
  $order_arg = array(
    '#entity' => 'uc_order',
    '#title' => t('Order'),
  );

  $actions['transportable_confirm_reservations'] = array(
    '#title' => t('Mark seat reservations as confirmed.'),
    '#category' => t('Order'),
    '#callback' => 'transportable_confirm_reservations',
    '#arguments' => array(
      'order' => $order_arg,
    ),
  );

  return $actions;
}

/**
 * Fires when ca action transportable_confirm_reservations is called
*/
function transportable_confirm_reservations($order) {
  transportable_inc('util');
  if (is_array($order->products)) {
    foreach ($order->products as $product) {
      if ($product->data['module'] == 'transportable' &&
          ($sids = $product->data['sids']) &&
          is_array($sids)) {
        foreach ($sids as $tsis_id => $seat) {
          transportable_seat_sold($tsis_id, $seat);
        }
      }
    }
  }
}
