<?php
// $Id$

/*
 * @file transportable.permissions.inc
 * provides permissions forms
 * @copyright Copyright(c) 2010 Lee Rowlands
 * @license GPL v3 http://www.fsf.org/licensing/licenses/gpl.html 
 * @author Lee Rowlands leerowlands at rowlands-bcs dot com
 * 
 */

/**
 * Form builder function
 * @param $form_state array std FAPI form state var
 * @return array form
*/
function transportable_permissions_form($form_state) {
  global $user;
  transportable_inc('util');
  $form = array();
    
  $coworkers = _transportable_coworkers();
  //unset current user
  unset($coworkers[$user->uid]);
  unset($coworkers[1]);//no administrator access of course!
  
  $stop_points = _transportable_get_stop_points();
  
  $permissions = array(
    t('Administer transportable methods'),
    t('Administer transportable services'),
    t('Administer staff'),
    t('Administer orders'),
    t('Administer stop points'),
  );
  
  $permissions = drupal_map_assoc($permissions);
  
  if (!count($coworkers)) {
    $form['message'] = array(
      '#value' => t('You do not have any staff to manage permissions for')
    );
    return $form;
  }
  
  foreach ($coworkers as $uid => $account) {
    $form['user_'. $uid] = array(
      '#type'         => 'fieldset',
      '#collapsible'  => TRUE,
      '#tree'         => FALSE,
      '#title'        => t('Permissions for @name', array('@name' => $account->name)),
      '#collapsed'    => FALSE,
      '#group'        => 'transportable'
    );
    
    $form['user_'. $uid]['permissions'] = array(
      '#tree' =>  TRUE
    );
    
    $form['user_'. $uid]['permissions'][$uid] = array(
      '#tree' =>  TRUE
    );
    
    $form['user_'. $uid]['permissions'][$uid]['id'] = array(
      '#type' => 'value',
      '#value' => $account->transportable_permissions->id,
    );
    
    $form['user_'. $uid]['permissions'][$uid]['general'] = array(
      '#type' => 'checkboxes',
      '#title' => t('General permissions'),
      '#default_value' => $account->transportable_permissions->permissions['general'] ?
                          $account->transportable_permissions->permissions['general'] :
                          array(),
      '#options' => $permissions,
      '#description' => t('Select the permissions to grant to the user'),
    );
    
    $form['user_'. $uid]['permissions'][$uid]['stop_points'] = array(
      '#type' => 'checkboxes',
      '#title' => t('General permissions'),
      '#default_value' => $account->transportable_permissions->permissions['stop_points'] ?
                          $account->transportable_permissions->permissions['stop_points'] :
                          array(),
      '#options' => $stop_points,
      '#description' => t("Select the stop point permissions to grant to the user, note that
                          these permissions will have no effect if the user has 'Administer stop points' general permission"),
    );
    
  }
  
  if (function_exists('vertical_tabs_form_pre_render')) {
    $form['#pre_render'][] = 'vertical_tabs_form_pre_render';
  }
  
  $form['save'] = array('#type' => 'submit', '#value' => t('Save'));
  
  return $form;
}

/**
 * Submission
*/
function transportable_permissions_form_submit($form, &$form_state) {
  foreach ($form_state['values']['permissions'] as $uid => $values) {
    $update = array();
    $record = array(
      'uid' => $uid,
      'permissions' => serialize($values)
    );
    if ($values['id']) {
      $record['id'] = $values['id'];
      $update = array('id');
    }
    drupal_write_record('transportable_permissions', $record, $update);
  }
}